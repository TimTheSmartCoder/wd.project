﻿using FluentValidation;
using WD.API.Licenses.Application.Commands.Requests;

namespace WD.API.Licenses.Application.Commands.Validators
{
    public class CreateLicenseRequestValidator
        : AbstractValidator<CreateLicenseRequest>
    {
        public CreateLicenseRequestValidator()
        {
            RuleFor(x => x.Model)
                .NotNull()
                .WithMessage("Model is not supplied, but it is required.");

            When(x => x.Model != null, () => {

                RuleFor(x => x.Model.Id)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Id can't be less than zero.");

                RuleFor(x => x.Model.Customer)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Customer can't be less than zero.");

                RuleFor(x => x.Model.User)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("User can't be less than zero.");

                When(x => x.Model.Cancelled != null, () => {

                    RuleFor(r => r.Model)
                        .Must(x => x.Activated < x.Cancelled)
                        .WithMessage("Activation time can't be after cancellation time.");
                });
            });
        }
    }
}
