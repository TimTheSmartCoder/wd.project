﻿using MediatR;
using System;
using WD.API.Licenses.Application.Commands.Models;
using WD.API.Licenses.Application.Commands.Results;

namespace WD.API.Licenses.Application.Commands.Requests
{
    public class UpdateLicenseRequest
        : IRequest<UpdateLicenseResult>
    {
        /// <summary>
        /// Construct's <see cref="UpdateLicenseRequest"/> with the given informaiton.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        public UpdateLicenseRequest(UpdateLicenseModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            Model = model;
        }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public UpdateLicenseModel Model { get; }
    }
}
