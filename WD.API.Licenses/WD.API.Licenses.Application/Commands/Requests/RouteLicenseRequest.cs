﻿using MediatR;
using System;
using WD.API.Licenses.Application.Commands.Models;
using WD.API.Licenses.Application.Commands.Results;

namespace WD.API.Licenses.Application.Commands.Requests
{
    public class RouteLicenseRequest
        : IRequest<RouteLicenseResult>
    {
        /// <summary>
        /// Construct's <see cref="RouteLicenseRequest"/> with the given information.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        /// <param name="correlationId">Correlation id with the request.</param>
        public RouteLicenseRequest(RouteLicenseModel model, string correlationId)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            if (correlationId == null)
                throw new ArgumentNullException(nameof(correlationId));

            CorrelationId = correlationId;
            Model = model;
        }

        /// <summary>
        /// Correlation id of the request.
        /// </summary>
        public string CorrelationId { get; }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public RouteLicenseModel Model { get; }
    }
}
