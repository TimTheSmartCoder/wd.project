﻿using System;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;

namespace WD.API.Licenses.Application.Commands.Models
{
    public class RouteLicenseModel
    {
        /// <summary>
        /// Id of the license.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id of the customer which owns the license.
        /// </summary>
        public int Customer { get; set; }

        /// <summary>
        /// Id of the user, which are connected to the license.
        /// </summary>
        public int User { get; set; }

        /// <summary>
        /// <see cref="DateTime"/> of the activation in UTC.
        /// </summary>
        public DateTime Activated { get; set; }

        /// <summary>
        /// <see cref="DateTime"/> of cancellation in UTC.
        /// </summary>
        public DateTime? Cancelled { get; set; }

        /// <summary>
        /// <see cref="LicenseType"/> of the <see cref="License"/>.
        /// </summary>
        public LicenseType Type { get; set; }

        /// <summary>
        /// UTC time of last modification.
        /// </summary>
        public DateTime Modified { get; set; }
    }
}
