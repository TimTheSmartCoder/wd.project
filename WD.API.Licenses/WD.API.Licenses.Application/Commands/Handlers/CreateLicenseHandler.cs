﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Application.Commands.Requests;
using WD.API.Licenses.Application.Commands.Results;
using WD.API.Licenses.Application.Commands.Validators;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.Core;
using WD.API.Licenses.Domain.UnitOfWorks;

namespace WD.API.Licenses.Application.Commands.Handlers
{
    public class CreateLicenseHandler
        : IRequestHandler<CreateLicenseRequest, CreateLicenseResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<CreateLicenseHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<CreateLicenseRequest> _validator;

        public CreateLicenseHandler(IUnitOfWork unitOfWork, ILogger<CreateLicenseHandler> logger)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _unitOfWork = unitOfWork;
            _logger = logger;

            _validator = new CreateLicenseRequestValidator();
        }

        public async Task<CreateLicenseResult> Handle(CreateLicenseRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Start processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validated request successfully.");

            _logger.LogInformation("Starts checking if license exist with id: '{id}'.", request.Model.Id);

            // Try to get license.
            License license = await _unitOfWork.Repositories
                .Licenses
                .GetAsync(request.Model.Id, cancellationToken);

            if (license != null)
            {
                _logger.LogWarning("License already exist with id: '{id}', skipping creation.", request.Model.Id);

                throw new BadRequestException($"License already exist with id: '{request.Model.Id}'.");
            }

            _logger.LogInformation("No license were found with id: '{id}'.", request.Model.Id);

            _logger.LogInformation("Creating license with id: '{id}'.");

            license = new License(
                request.Model.Id,
                request.Model.Customer,
                request.Model.User,
                request.Model.Type,
                request.Model.Activated,
                request.Model.Modified);

            if (request.Model.Cancelled != null)
                license.SetCancelled(request.Model.Cancelled.GetValueOrDefault());

            // Add license to repository.
             _unitOfWork.Repositories.Licenses.Insert(license);

            // Save all changes.
            await _unitOfWork.SaveChanges(cancellationToken);

            _logger.LogInformation("Finished request successfully.");

            CreateLicenseResult result = new CreateLicenseResult()
            {
                Id = license.Id,
                Activated = license.Activated,
                Cancelled = license.Cancelled,
                Customer = license.Customer,
                Modified = license.Modified,
                Type = license.Type,
                User = license.User
            };

            return result;
        }
    }
}
