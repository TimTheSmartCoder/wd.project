﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WD.API.Licenses.Application.Commands.Models;
using WD.API.Licenses.Application.Commands.Requests;
using WD.API.Licenses.Application.Commands.Results;
using WD.API.Licenses.Application.Commands.Validators;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.UnitOfWorks;

namespace WD.API.Licenses.Application.Commands.Handlers
{
    public class RouteLicenseHandler
        : IRequestHandler<RouteLicenseRequest, RouteLicenseResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<RouteLicenseHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<RouteLicenseRequest> _validator;

        /// <summary>
        /// <see cref="IMediator"/> to use.
        /// </summary>
        private readonly IMediator _mediator;

        /// <summary>
        /// <see cref="ITopicClient"/> to use.
        /// </summary>
        private readonly ITopicClient _topicClient;

        public RouteLicenseHandler(
            IUnitOfWork unitOfWork, 
            ILogger<RouteLicenseHandler> logger, 
            IMediator mediator,
            IConfiguration configuration)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
            if (mediator == null)
                throw new ArgumentNullException(nameof(mediator));

            _unitOfWork = unitOfWork;
            _logger = logger;
            _mediator = mediator;

            // Create client for publish topics.
            _topicClient = new TopicClient(configuration["Azure:ServiceBus:ConnectionStrings:Default"], "License");

            _validator = new RouteLicenseRequestValidator();
        }

        public async Task<RouteLicenseResult> Handle(RouteLicenseRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Start processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validated request successfully.");

            _logger.LogInformation("Starts checking if license exist with id: '{id}'.", request.Model.Id);

            // Try to get license.
            License license = await _unitOfWork.Repositories
                .Licenses
                .GetAsync(request.Model.Id, cancellationToken);


            if (license == null)
            {
                _logger.LogInformation("License with id: '{Id}' do not exist, creating it.", request.Model.Id);
                
                // Prepare model for creation of license.
                CreateLicenseModel model = new CreateLicenseModel()
                {
                    Id = request.Model.Id,
                    Activated = request.Model.Activated,
                    Cancelled = request.Model.Cancelled,
                    Customer = request.Model.Customer,
                    User = request.Model.User,
                    Type = request.Model.Type,
                    Modified = request.Model.Modified
                };

                // Send request for creating license.
                CreateLicenseResult result = await _mediator.Send(new CreateLicenseRequest(model));

                // Serialize the result.
                string resultAsJson = JsonConvert.SerializeObject(result);

                // Put the result into the message body.
                Message message = new Message(Encoding.UTF8.GetBytes(resultAsJson));

                // Indicate that the license has been created.
                message.Label = "Created";

                // Set correlation id.
                message.CorrelationId = request.CorrelationId;

                // Set content type.
                message.ContentType = "application/json";

                // Send message.
                await _topicClient.SendAsync(message);

                await _topicClient.CloseAsync();
            }
            else
            {
                _logger.LogInformation("License with id: '{Id}' exist, updating it.", request.Model.Id);

                if (request.Model.Modified < license.Modified)
                {
                    _logger.LogInformation("License with id: '{Id}' exist, modication time is newer than this request, skipping update.", request.Model.Id);
                }
                else
                {
                    // Prepare model for updating license.
                    UpdateLicenseModel model = new UpdateLicenseModel()
                    {
                        Id = request.Model.Id,
                        Activated = request.Model.Activated,
                        Cancelled = request.Model.Cancelled,
                        Customer = request.Model.Customer,
                        User = request.Model.User,
                        Type = request.Model.Type,
                        Modified = request.Model.Modified
                    };

                    // Send request for updating license.
                    UpdateLicenseResult result = await _mediator.Send(new UpdateLicenseRequest(model));

                    // Serialize the result.
                    string resultAsJson = JsonConvert.SerializeObject(result);

                    // Put the result into the message body.
                    Message message = new Message(Encoding.UTF8.GetBytes(resultAsJson));

                    // Indicate that the license has been created.
                    message.Label = "Updated";

                    // Set correlation id.
                    message.CorrelationId = request.CorrelationId;

                    // Set content type.
                    message.ContentType = "application/json";

                    // Send message.
                    await _topicClient.SendAsync(message);

                    await _topicClient.CloseAsync();
                }
            }

            return new RouteLicenseResult();
        }
    }
}
