﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Application.Commands.Requests;
using WD.API.Licenses.Application.Commands.Results;
using WD.API.Licenses.Application.Commands.Validators;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.Core;
using WD.API.Licenses.Domain.UnitOfWorks;

namespace WD.API.Licenses.Application.Commands.Handlers
{
    public class UpdateLicenseHandler
        : IRequestHandler<UpdateLicenseRequest, UpdateLicenseResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<UpdateLicenseHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<UpdateLicenseRequest> _validator;

        public UpdateLicenseHandler(IUnitOfWork unitOfWork, ILogger<UpdateLicenseHandler> logger)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _unitOfWork = unitOfWork;
            _logger = logger;

            _validator = new UpdateLicenseRequestValidator();
        }

        public async Task<UpdateLicenseResult> Handle(UpdateLicenseRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Start processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validated request successfully.");

            _logger.LogInformation("Starts checking if license exist with id: '{id}'.", request.Model.Id);

            // Try to get license.
            License license = await _unitOfWork.Repositories
                .Licenses
                .GetAsync(request.Model.Id, cancellationToken);

            if (license == null)
            {
                _logger.LogWarning("License do not exist with id: '{id}'.", request.Model.Id);

                throw new BadRequestException($"License do not exist with id: '{request.Model.Id}'.");
            }

            _logger.LogInformation("License were found with id: '{id}'.", request.Model.Id);

            _logger.LogInformation("Updating license with id: '{id}'.");

            license.SetCustomer(request.Model.Customer);
            license.SetUser(request.Model.User);
            license.SetType(request.Model.Type);
            license.SetActivated(request.Model.Activated);
            license.SetModified(request.Model.Modified);

            if (request.Model.Cancelled != null)
                license.SetCancelled(request.Model.Cancelled.GetValueOrDefault());

            // Update the license in repository.
            _unitOfWork.Repositories.Licenses.Update(license);

            // Save all changes.
            await _unitOfWork.SaveChanges(cancellationToken);

            _logger.LogInformation("Finished request successfully.");

            UpdateLicenseResult result = new UpdateLicenseResult()
            {
                Id = license.Id,
                Activated = license.Activated,
                Cancelled = license.Cancelled,
                Customer = license.Customer,
                Modified = license.Modified,
                Type = license.Type,
                User = license.User
            };
            
            return result;
        }
    }
}
