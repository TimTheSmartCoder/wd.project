﻿using System;

namespace WD.API.Licenses.Domain.Core
{
    public class DomainException
        : Exception
    {
        /// <summary>
        /// Construct's <see cref="DomainException"/> with the given information.
        /// </summary>
        /// <param name="message">Message of the <see cref="DomainException"/>.</param>
        public DomainException(string message) 
            : base(message)
        { }

        /// <summary>
        /// Construct's <see cref="DomainException"/> with the given information.
        /// </summary>
        /// <param name="message">Message of the <see cref="DomainException"/>.</param>
        /// <param name="innerException">Inner exception of the <see cref="DomainException"/>.</param>
        public DomainException(string message, Exception innerException) 
            : base(message, innerException)
        { }
    }
}
