﻿using System;
using WD.API.Licenses.Domain.Core;

namespace WD.API.Licenses.Domain.Aggregates.LicenseAggregate
{
    public class License
        : IAggregate
    {
        /// <summary>
        /// Construct's <see cref="License"/> with default information.
        /// (Only exist for EFCore support).
        /// </summary>
        private License() { }

        /// <summary>
        /// Construct's <see cref="License"/> with the given information.
        /// </summary>
        /// <param name="id">Id of the <see cref="License"/>.</param>
        /// <param name="customer">Id of the customer which owns the <see cref="License"/>.</param>
        /// <param name="user">Id of the user.</param>
        /// <param name="type">Type of <see cref="License"/>.</param>
        /// <param name="activationTime">UTC time of activation.</param>
        /// <param name="Modified">Modification time in UTC.</param>
        public License(int id, int customer, int user, LicenseType type, DateTime activationTime, DateTime modified)
            : this()
        {
            if (id < 0)
                throw new ArgumentOutOfRangeException(nameof(id), "Can't be less than zero.");
            
            Id = id;

            SetCustomer(customer);
            SetUser(user);
            SetType(type);
            SetModified(modified);

            // Set activation time.
            SetActivated(activationTime);
        }

        /// <summary>
        /// Construct's <see cref="License"/> with the given information.
        /// </summary>
        /// <param name="customer">Id of the customer which owns the <see cref="License"/>.</param>
        /// <param name="user">Id of the user.</param>
        /// <param name="type">Type of <see cref="License"/>.</param>
        /// <param name="activationTime">UTC time of activation.</param>
        /// <param name="Modified">Modification time in UTC.</param>
        public License(int customer, int user, LicenseType type, DateTime activationTime, DateTime modified)
            : this(0, customer, user, type, activationTime, modified)
        { }

        /// <summary>
        /// Primary key of the aggregate.
        /// </summary>
        public int Key { get; private set; }

        /// <summary>
        /// Id of the <see cref="License"/>.
        /// </summary>
        public int Id { get; private set; }
        
        /// <summary>
        /// Id of the Customer who owns the license.
        /// </summary>
        public int Customer { get; private set; }

        /// <summary>
        /// Id of the user, which have the license.
        /// </summary>
        public int User { get; private set; }

        /// <summary>
        /// <see cref="DateTime"/> of the activation in UTC.
        /// </summary>
        public DateTime Activated { get; private set; }

        /// <summary>
        /// <see cref="DateTime"/> of cancellation in UTC.
        /// </summary>
        public DateTime? Cancelled { get; private set; }
        
        /// <summary>
        /// <see cref="LicenseStatus"/> of the <see cref="License"/>.
        /// </summary>
        public LicenseStatus Status { get; private set; }

        /// <summary>
        /// <see cref="LicenseType"/> of the <see cref="License"/>.
        /// </summary>
        public LicenseType Type { get; private set; }
        
        /// <summary>
        /// UTC time of last modification.
        /// </summary>
        public DateTime Modified { get; private set; }

        /// <summary>
        /// Set's the modification time.
        /// </summary>
        /// <param name="modified">UTC time of last modification.</param>
        public void SetModified(DateTime modified)
        {
            Modified = modified;
        }

        /// <summary>
        /// Sets the license type of the <see cref="License"/>.
        /// </summary>
        /// <param name="type">Type of license.</param>
        public void SetType(LicenseType type)
        {
            Type = type;
        }

        /// <summary>
        /// Set's the user of the <see cref="License"/>.
        /// </summary>
        /// <param name="user">Id of the user.</param>
        public void SetUser(int user)
        {
            if (user < 0)
                throw new ArgumentOutOfRangeException(nameof(user), "Can't be less than zero.");

            User = user;
        }

        /// <summary>
        /// Set's the customer of the <see cref="License"/>.
        /// </summary>
        /// <param name="customer">Id of the customer.</param>
        public void SetCustomer(int customer)
        {
            if (customer < 0)
                throw new ArgumentOutOfRangeException(nameof(customer), "Can't be less than zero.");

            Customer = customer;
        }

        /// <summary>
        /// Set's the activation time of the license.
        /// </summary>
        /// <param name="activationTime">Time of activation.</param>
        public void SetActivated(DateTime activationTime)
        {
            if (Cancelled != null && activationTime > Cancelled)
                throw new DomainException("Activation time can't be after cancellation.");

            Activated = activationTime;

            // If cancellation time is not set, status is activated.
            if (Cancelled == null)
                Status = LicenseStatus.Activated;
        }

        /// <summary>
        /// Set's the cancellation time of the license.
        /// </summary>
        /// <param name="cancellationTime">UTC time of cancellation.</param>
        public void SetCancelled(DateTime cancellationTime)
        {
            if (Activated != null && cancellationTime < Activated)
                throw new DomainException("Cancellation time can't be before activation.");
            
            Cancelled = cancellationTime;

            // Set the status to cancelled.
            Status = LicenseStatus.Cancelled;
        }
    }

    public enum LicenseType
    {
        /// <summary>
        /// Mobile version license.
        /// </summary>
        Mobile = 10,
        
        /// <summary>
        /// Light version license.
        /// </summary>
        Light = 20,
        
        /// <summary>
        /// Pro version license.
        /// </summary>
        Pro = 30
    }

    public enum LicenseStatus
    {
        Activated = 10,
        Cancelled = 20,
    }
}
