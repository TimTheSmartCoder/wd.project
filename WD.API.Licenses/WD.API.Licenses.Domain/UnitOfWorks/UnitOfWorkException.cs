﻿using System;

namespace WD.API.Licenses.Domain.UnitOfWorks
{
    public class UnitOfWorkException
        : Exception
    {
        public UnitOfWorkException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
