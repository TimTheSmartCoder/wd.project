﻿using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.UnitOfWorks.Repositories;

namespace WD.API.Licenses.Domain.UnitOfWorks
{
    /// <summary>
    /// <see cref="IUnitOfWork"/> for represening a unit of work.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// List of repositories available in the <see cref="IUnitOfWork"/>.
        /// </summary>
        IRepositoryList Repositories { get; }

        /// <summary>
        /// Save all changes applied to the <see cref="IUnitOfWork"/>.
        /// </summary>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> for cancelling operation.</param>
        /// <returns>True if success, otherwise false.</returns>
        /// <exception cref="UnitOfWorkException">Throws exception if fails to save changes.</exception>
        Task<bool> SaveChanges(CancellationToken cancellationToken = default(CancellationToken));
    }

    /// <summary>
    /// List of <see cref="IRepository"/> available in <see cref="IUnitOfWork"/>.
    /// </summary>
    public interface IRepositoryList
    {
        /// <summary>
        /// Repository for <see cref="License"/>.
        /// </summary>
        ILicenseRepository Licenses { get; }
    }
}
