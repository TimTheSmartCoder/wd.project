﻿using WD.API.Licenses.Domain.Core;

namespace WD.API.Licenses.Domain.UnitOfWorks.Repositories
{
    /// <summary>
    /// <see cref="IRepository{TAggregate}"/> representing repository for
    /// the specified <see cref="IAggregate"/>.
    /// </summary>
    /// <typeparam name="TAggregate"></typeparam>
    public interface IRepository<TAggregate>
        where TAggregate : IAggregate
    { }
}
