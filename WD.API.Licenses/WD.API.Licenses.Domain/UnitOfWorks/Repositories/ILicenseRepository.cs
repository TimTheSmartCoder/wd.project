﻿using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;

namespace WD.API.Licenses.Domain.UnitOfWorks.Repositories
{
    public interface ILicenseRepository
        : IRepository<License>
    {
        /// <summary>
        /// Checks if an <see cref="License"/> exist with the
        /// given information.
        /// </summary>
        /// <param name="id">Id of the <see cref="License"/>.</param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> for cancelling task.</param>
        /// <returns>True if <see cref="License"/> exist, otherwise false.</returns>
        Task<bool> ExistsAsync(int id, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Gets <see cref="License"/> with the given information.
        /// </summary>
        /// <param name="id">Id of the <see cref="License"/> to get.</param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> for cancelling task.</param>
        /// <returns><see cref="License"/> if exist, otherwise null.</returns>
        Task<License> GetAsync(int id, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Inserts the given <see cref="License"/>.
        /// </summary>
        /// <param name="license"><see cref="License"/> to insert.</param>
        /// <returns>Inserted <see cref="License"/>.</returns>
        License Insert(License license);
        
        /// <summary>
        /// Updates the given <see cref="License"/>.
        /// </summary>
        /// <param name="license"><see cref="License"/> to update.</param>
        /// <returns>Updated <see cref="License"/>.</returns>
        License Update(License license);
    }
}
