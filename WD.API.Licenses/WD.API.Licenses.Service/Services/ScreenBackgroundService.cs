﻿using MediatR;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Application.Commands.Models;
using WD.API.Licenses.Application.Commands.Requests;

namespace WD.API.Licenses.Service.Services
{
    public class ScreenBackgroundService
        : BackgroundService
    {
        /// <summary>
        /// Name of the topic, which the service is subscribing on.
        /// </summary>
        private static readonly string _topic = "Screen";

        /// <summary>
        /// Name of the subscription, which the service is using in subscription.
        /// </summary>
        private static readonly string _subscription = "License";

        /// <summary>
        /// <see cref="IServiceProvider"/> to use.
        /// </summary>
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// <see cref="ISubscriptionClient"/> to use.
        /// </summary>
        private readonly ISubscriptionClient _subscriptionClient;


        public ScreenBackgroundService(IServiceProvider serviceProvider)
            : base()
        {
            if (serviceProvider == null)
                throw new ArgumentNullException(nameof(serviceProvider));

            _serviceProvider = serviceProvider;

            // Get configuration from dependency injection.
            IConfiguration configuration = _serviceProvider
                .GetRequiredService<IConfiguration>();

            // Get logger from dependency injection.
            ILogger<ScreenBackgroundService> logger = _serviceProvider
                .GetRequiredService<ILogger<ScreenBackgroundService>>();

            logger.LogInformation("Starting background service.");

            // Create subscription client.
            _subscriptionClient = new SubscriptionClient(
                configuration["Azure:ServiceBus:ConnectionStrings:Default"],
                _topic,
                _subscription);

            _subscriptionClient.RemoveRuleAsync(RuleDescription.DefaultRuleName);

            // Add filter for allowing messages with label of created to go through.
            _subscriptionClient.AddRuleAsync(new RuleDescription()
            {
                Filter = new CorrelationFilter() { Label = "Added" },
                Name = "LabelCreatedRule"
            });

            // Add filter for allowing messages with label of updated to go through.
            _subscriptionClient.AddRuleAsync(new RuleDescription()
            {
                Filter = new CorrelationFilter() { Label = "Released" },
                Name = "LabelUpdatedRule"
            });

            logger.LogInformation("{name} created with topic: '{topic}' and subscription: '{subscription}'.",
                nameof(ISubscriptionClient),
                _topic,
                _subscription);
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            ILogger<ScreenBackgroundService> logger = _serviceProvider
                .GetRequiredService<ILogger<ScreenBackgroundService>>();

            logger.LogInformation("Registering message and error handler.");

            _subscriptionClient.RegisterMessageHandler(
                OnMessage,
                new MessageHandlerOptions(OnError) { AutoComplete = false, MaxConcurrentCalls = 1 });

            logger.LogInformation("Registered message and error handler.");

            logger.LogInformation("Waiting for cancellation of background service.");

            // Wait for cancellation.
            await Task.WhenAny(Task.Delay(Timeout.Infinite, cancellationToken));

            logger.LogInformation("Cancellation of background service requested.");

            logger.LogInformation("Closing subscription for topic: '{topic}' and 'subscription'. ", _topic, _subscription);

            // Close subscription client.
            await _subscriptionClient.CloseAsync();

            logger.LogInformation("Closed subscription for topic: '{topic}' and 'subscription'. ", _topic, _subscription);
        }

        private async Task OnMessage(Message message, CancellationToken cancellationToken)
        {
            // Create scope, because each call to handle in asynchronous, which means potentially different threads.
            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                ILogger<ScreenBackgroundService> logger = scope.ServiceProvider
                    .GetRequiredService<ILogger<ScreenBackgroundService>>();

                using (logger.BeginScope($"Request Id: '{message.MessageId}' and CorrelationId: '{message.CorrelationId}'."))
                {
                    if (!string.Equals(message.ContentType, "application/json", StringComparison.OrdinalIgnoreCase))
                    {
                        logger.LogError("Unable to process message, because content type is not 'application/json'.");

                        return;
                    }

                    logger.LogInformation("Starts processing message.");

                    // Decode the body.
                    string decodedBody = Encoding.UTF8.GetString(message.Body);

                    // Get model from the message.
                    RouteLicenseModel model
                        = JsonConvert.DeserializeObject<RouteLicenseModel>(decodedBody);

                    IMediator mediator = scope.ServiceProvider
                        .GetRequiredService<IMediator>();

                    // Process message.
                    await mediator.Send(new RouteLicenseRequest(model, message.CorrelationId));

                    logger.LogInformation("Finished processing message.");

                    await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
                }
            }
        }

        private Task OnError(ExceptionReceivedEventArgs args)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var logger = scope.ServiceProvider
                    .GetRequiredService<ILogger<ScreenBackgroundService>>();

                logger.LogError(
                    args.Exception,
                    "Unexpected exception occured while listening for topic: '{topic}' with subscription: '{subscription}'.",
                    _topic,
                    _subscription);
            }

            return Task.CompletedTask;
        }
    }
}
