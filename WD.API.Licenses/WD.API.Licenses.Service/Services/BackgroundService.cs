﻿using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace WD.API.Licenses.Service.Services
{
    /// <summary>
    /// Class constructed with inspiration from the following resource:
    /// https://github.com/aspnet/Hosting/blob/ca0bd0deaeef872af7a1a13bb7b8bc041ecdc183/src/Microsoft.Extensions.Hosting.Abstractions/BackgroundService.cs
    /// </summary>
    public abstract class BackgroundService
        : IHostedService, IDisposable
    {
        /// <summary>
        /// <see cref="Task"/> to execute in the background.
        /// </summary>
        private Task _task;

        /// <summary>
        /// <see cref="CancellationTokenSource"/> for managing <see cref="CancellationToken"/> for
        /// the background task.
        /// </summary>
        private readonly CancellationTokenSource _cancellationTokenSource 
            = new CancellationTokenSource();

        /// <summary>
        /// This method is called when the <see cref="IHostedService"/> starts. The implementation should return a task that represents
        /// the lifetime of the long running operation(s) being performed.
        /// </summary>
        /// <param name="cancellationToken">Triggered when <see cref="IHostedService.StopAsync(CancellationToken)"/> is called.</param>
        /// <returns>A <see cref="Task"/> that represents the long running operations.</returns>
        protected abstract Task ExecuteAsync(CancellationToken cancellationToken);

        public Task StartAsync(CancellationToken cancellationToken)
        {
            // Start executing background task.
            _task = ExecuteAsync(_cancellationTokenSource.Token);

            // If task has completed return.
            if (_task.IsCompleted)
                return _task;

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_task == null)
                return;

            try
            {
                // Signal cancellation to the background task.
                _cancellationTokenSource.Cancel();
            }
            finally
            {
                // Wait until the background task completes or until the stop cancellation
                // token signal stop.
                await Task.WhenAny(_task, Task.Delay(Timeout.Infinite, cancellationToken));
            }
        }

        public void Dispose()
        {
            // Cancel any running background task.
            _cancellationTokenSource.Cancel();
        }
    }
}
