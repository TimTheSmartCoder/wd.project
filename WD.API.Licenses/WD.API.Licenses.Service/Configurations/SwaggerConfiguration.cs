﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace WD.API.Licenses.Service.Configurations
{
    public static class SwaggerConfiguration
    {
        /// <summary>
        /// Configure's Swagger.
        /// </summary>
        public static IServiceCollection ConfigureSwagger(
            this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (!bool.Parse(configuration["Documentation:Enabled"]))
                return serviceCollection;

            // Register the Swagger generator, defining 1 or more Swagger documents
            serviceCollection.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "WD.API.Licenses", Version = "v1" });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            return serviceCollection;
        }

        /// <summary>
        /// Configure's Swagger.
        /// </summary>
        public static IApplicationBuilder ConfigureSwagger(
            this IApplicationBuilder applicationBuilder, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Enable swagger if documentation is turned on.
            if (!bool.Parse(configuration["Documentation:Enabled"]))
                return applicationBuilder;

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            applicationBuilder.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            applicationBuilder.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "WD.API.Videos V1");
            });

            return applicationBuilder;
        }
    }
}
