﻿using CorrelationId;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace WD.API.Licenses.Service.Configurations
{
    public static class CorrelationIdConfiguration
    {
        /// <summary>
        /// Configure's correlation id.
        /// </summary>
        public static IServiceCollection ConfigureCorrelationId(
            this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            serviceCollection.AddCorrelationId();

            return serviceCollection;
        }

        /// <summary>
        /// Configure's correlation id.
        /// </summary>
        public static IApplicationBuilder ConfigureCorrelationId(
            this IApplicationBuilder applicationBuilder, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            applicationBuilder.UseCorrelationId();

            return applicationBuilder;
        }
    }
}
