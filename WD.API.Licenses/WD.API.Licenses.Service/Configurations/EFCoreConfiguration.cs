﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore;

namespace WD.API.Licenses.Service.Configurations
{
    public static class EFCoreConfiguration
    {
        /// <summary>
        /// Configure's EFCore.
        /// </summary>
        public static IServiceCollection ConfigureEfCore(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Setup Db context using the connection string. A more ideal solution here would be
            // to use AddDbContextPool but as of now EFCore AddDbContextPool do not support
            // setting ServiceLifeTime.
            serviceCollection.AddDbContext<LicensesDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("Default")), ServiceLifetime.Transient);

            return serviceCollection;
        }
    }
}
