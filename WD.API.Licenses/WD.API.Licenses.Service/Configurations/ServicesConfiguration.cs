﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WD.API.Licenses.Domain.UnitOfWorks;
using WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Licenses.Service.Services;

namespace WD.API.Licenses.Service.Configurations
{
    public static class ServicesConfiguration
    {
        /// <summary>
        /// Configures services.
        /// </summary>
        public static IServiceCollection ConfigureServices(
            this IServiceCollection serviceCollection, 
            IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            serviceCollection.AddTransient<IUnitOfWork, UnitOfWork>();

            // Enabled background service, if enabled.
            if (bool.Parse(configuration["BackgroundServices:Enabled"]))
            {
                // Add background services.
                serviceCollection.AddHostedService<ScreenBackgroundService>();
            }


            return serviceCollection;
        }
    }
}
