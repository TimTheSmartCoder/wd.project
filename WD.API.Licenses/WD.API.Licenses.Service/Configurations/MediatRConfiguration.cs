﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WD.API.Licenses.Application;

namespace WD.API.Licenses.Service.Configurations
{
    public static class MediatRConfiguration
    {
        /// <summary>
        /// Configure's <see cref="IMediator"/>.
        /// </summary>
        public static IServiceCollection ConfigureMediatR(
            this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Setup MediatR and tell it to look for commands and handlers in the Application project.
            serviceCollection.AddMediatR(typeof(Assembly));

            return serviceCollection;
        }
    }
}
