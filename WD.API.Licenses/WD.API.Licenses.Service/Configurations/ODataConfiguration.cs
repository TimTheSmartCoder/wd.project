﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;

namespace WD.API.Licenses.Service.Configurations
{
    public static class ODataConfiguration
    {
        /// <summary>
        /// Configure's OData support.
        /// </summary>
        public static IServiceCollection ConfigureOData(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Add OData support.
            serviceCollection.AddOData();

            return serviceCollection;
        }

        /// <summary>
        /// Configure's health check.
        /// </summary>
        public static IApplicationBuilder ConfigureOData(
            this IApplicationBuilder applicationBuilder, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Create odata builder.
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            
            {
                // Add enum to the list of types odata should support.
                builder.AddEnumType(typeof(LicenseStatus));

                // Add licenses to the set of entities to support.
                var licenses = builder.EntitySet<License>("Licenses");

                // Set the key of an license.
                licenses.EntityType.HasKey(x => x.Id);

                // Add fiels to use in OData.
                licenses.EntityType.Property(x => x.Activated);
                licenses.EntityType.Property(x => x.Cancelled);
                licenses.EntityType.Property(x => x.Customer);
                licenses.EntityType.EnumProperty(x => x.Status);
            }


            applicationBuilder.UseMvc(b =>
            {
                b.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
            });

            return applicationBuilder;
        }
    }
}
