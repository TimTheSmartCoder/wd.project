﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore;

namespace WD.API.Licenses.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LicensesController : ControllerBase
    {
        /// <summary>
        /// <see cref="LicensesDbContext"/> to use.
        /// </summary>
        private readonly LicensesDbContext _licensesDbContext;

        public LicensesController(LicensesDbContext licensesDbContext)
        {
            if (licensesDbContext == null)
                throw new ArgumentNullException(nameof(licensesDbContext));

            _licensesDbContext = licensesDbContext;
        }

        [EnableQuery]
        public IActionResult Get()
        {
            return new OkObjectResult(_licensesDbContext.Set<License>());
        }

        [EnableQuery]
        public IActionResult Get(int key)
        {
            var k = _licensesDbContext.Set<License>().FirstOrDefault(x => x.Id == key);
            return new OkObjectResult(_licensesDbContext.Set<License>().FirstOrDefault(x => x.Id == key));
        }
    }
}