﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WD.API.Licenses.Service.Configurations;

namespace WD.API.Licenses.Service
{
    public class Startup
    {
        private IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Add Entity Framework Core configuration using extension.
            services.ConfigureEfCore(_configuration);

            // Add Health Check configuration using extension.
            services.ConfigureHealthChecks(_configuration);
            
            // Add MediatR configuration using extension.
            services.ConfigureMediatR(_configuration);

            // Add CorrelationId configuration using extension.
            services.ConfigureCorrelationId(_configuration);

            // Add Swagger configuration using extension.
            services.ConfigureSwagger(_configuration);

            // Add OData configuration using extension.
            services.ConfigureOData(_configuration);

            // Add services using extension.
            services.ConfigureServices(_configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Add CorrelationId configuration using extension.
            app.ConfigureCorrelationId(_configuration);

            // Add Health Check configuration using extension.
            app.ConfigureHealthChecks(_configuration);

            // Add Swagger configuration using extension.
            app.ConfigureSwagger(_configuration);

            // Add OData configuration using extension.
            app.ConfigureOData(_configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
