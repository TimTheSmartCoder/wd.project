﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;

namespace WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore.EntityTypeConfigurations
{
    public class LicenseEntityTypeConfiguration
        : IEntityTypeConfiguration<License>
    {
        public void Configure(EntityTypeBuilder<License> builder)
        {
            // Primary key.
            builder.HasKey(x => x.Key);

            // Set unique id.
            builder.HasIndex(x => x.Id).IsUnique();
        }
    }
}
