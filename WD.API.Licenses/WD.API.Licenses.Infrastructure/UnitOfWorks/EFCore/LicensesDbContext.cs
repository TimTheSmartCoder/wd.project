﻿using Microsoft.EntityFrameworkCore;
using WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore.EntityTypeConfigurations;

namespace WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore
{
    public class LicensesDbContext
        : DbContext
    {
        public LicensesDbContext(DbContextOptions<LicensesDbContext> dbContextOptions)
            : base(dbContextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new LicenseEntityTypeConfiguration());
        }
    }
}
