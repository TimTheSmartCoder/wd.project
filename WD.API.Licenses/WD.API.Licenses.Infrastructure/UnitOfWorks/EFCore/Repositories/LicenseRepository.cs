﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.UnitOfWorks.Repositories;

namespace WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore.Repositories
{
    public class LicenseRepository
        : ILicenseRepository
    {
        /// <summary>
        /// <see cref="LicensesDbContext"/> to use.
        /// </summary>
        private readonly LicensesDbContext _licensesDbContext;

        /// <summary>
        /// Construct's <see cref="LicenseRepository"/> with the given information.
        /// </summary>
        /// <param name="licensesDbContext"><see cref="LicensesDbContext"/> to use.</param>
        public LicenseRepository(LicensesDbContext licensesDbContext)
        {
            if (licensesDbContext == null)
                throw new ArgumentNullException(nameof(licensesDbContext));

            _licensesDbContext = licensesDbContext;
        }

        public async Task<bool> ExistsAsync(
            int id, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _licensesDbContext.Set<License>()
                .AnyAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<License> GetAsync(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _licensesDbContext.Set<License>()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public License Insert(License license)
        {
            if (license == null)
                throw new ArgumentNullException(nameof(license));

            var entity = _licensesDbContext.Set<License>()
                .Add(license)
                .Entity;

            return entity;
        }

        public License Update(License license)
        {
            if (license == null)
                throw new ArgumentNullException(nameof(license));

            var entity = _licensesDbContext.Set<License>()
                .Update(license)
                .Entity;

            return entity;
        }
    }
}
