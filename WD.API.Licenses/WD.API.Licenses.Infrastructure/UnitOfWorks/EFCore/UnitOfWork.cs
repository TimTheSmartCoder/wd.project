﻿using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Domain.UnitOfWorks;
using WD.API.Licenses.Domain.UnitOfWorks.Repositories;
using WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore.Repositories;

namespace WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore
{
    public class UnitOfWork
        : IUnitOfWork
    {
        /// <summary>
        /// <see cref="LicensesDbContext"/> to use.
        /// </summary>
        private readonly LicensesDbContext _licensesDbContext;

        /// <summary>
        /// Construct's <see cref="UnitOfWork"/> with the given
        /// information.
        /// </summary>
        /// <param name="licensesDbContext">DbContext to use.</param>
        public UnitOfWork(LicensesDbContext licensesDbContext)
        {
            if (licensesDbContext == null)
                throw new ArgumentNullException(nameof(licensesDbContext));

            _licensesDbContext = licensesDbContext;

            // Create repository list with all repositiories.
            Repositories = new RepositoryList(_licensesDbContext);
        }

        public IRepositoryList Repositories { get; }

        public async Task<bool> SaveChanges(
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                // Save all changes in the context.
                var result = await _licensesDbContext.SaveChangesAsync(cancellationToken);

                return true;
            }
            catch (Exception exception)
            {
                // Throw an new exception but wrapped in a unit of work exception.
                throw new UnitOfWorkException($"{nameof(UnitOfWork)} failed to save changes.", exception);
            }
        }
    }

    public class RepositoryList
        : IRepositoryList
    {
        /// <summary>
        /// <see cref="LicensesDbContext"/> to use.
        /// </summary>
        private readonly LicensesDbContext _licensesDbContext;

        /// <summary>
        /// Hidden field for <see cref="Licenses"/>.
        /// </summary>
        private ILicenseRepository _licenseRepository;

        /// <summary>
        /// Construct's <see cref="RepositoryList"/> with the given
        /// information.
        /// </summary>
        /// <param name="licensesDbContext"><see cref="LicensesDbContext"/> to use.</param>
        public RepositoryList(LicensesDbContext licensesDbContext)
        {
            if (licensesDbContext == null)
                throw new ArgumentNullException(nameof(licensesDbContext));

            _licensesDbContext = licensesDbContext;
        }

        public ILicenseRepository Licenses => 
            _licenseRepository ?? (_licenseRepository = new LicenseRepository(_licensesDbContext));
    }
}
