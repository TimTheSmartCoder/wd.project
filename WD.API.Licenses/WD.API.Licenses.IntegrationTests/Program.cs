﻿using System;
using System.Threading.Tasks;
using WD.API.Licenses.IntegrationTests.BackgroundServices;

namespace WD.API.Licenses.IntegrationTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Run(args).GetAwaiter().GetResult();
        }

        static async Task Run(string[] args)
        {
            // Connection string for Azure Service Bus to use under testing.
            string azureServiceBusConnectionString = "";

            // Database connection string to use under testing.
            string databaseConnectionString = "";

            // Create test.
            ScreenBackgrounServiceTests screenBackgrounServiceTests 
                = new ScreenBackgrounServiceTests(azureServiceBusConnectionString, databaseConnectionString);

            Console.WriteLine("Starting test of ScreenBackgroundService.");

            // Run test for screen background service.
            await screenBackgrounServiceTests.Run();

            Console.WriteLine();
            Console.WriteLine("Successfully tested ScreenBackgroundService.");

            Console.WriteLine();
            Console.WriteLine("All test are done.");
            Console.ReadLine();
        }
    }
}
