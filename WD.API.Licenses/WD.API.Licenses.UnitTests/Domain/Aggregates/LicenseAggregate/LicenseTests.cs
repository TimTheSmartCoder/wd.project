﻿using System;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.Core;
using Xunit;

namespace WD.API.Licenses.UnitTests.Domain.Aggregates.LicenseAggregate
{
    public class LicenseTests
    {
        [Fact]
        public void Should_ThrowArgumentOutOfRageException_When_IdIsLessThanZero()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new License(-1, 1, 1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow));
        }

        [Fact]
        public void Should_ThrowArgumentOutOfRageException_When_CustomerIsLessThanZero()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new License(1, -1, 1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow));
        }

        [Fact]
        public void Should_ThrowArgumentOutOfRageException_When_UserrIsLessThanZero()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new License(1, 1, -1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow));
        }

        [Fact]
        public void Should_ThrowDomainException_When_ActivatationTimeIsAfterCancellation()
        {
            License license = new License(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow);

            license.SetCancelled(DateTime.UtcNow);

            Assert.Throws<DomainException>(() => license.SetActivated(DateTime.UtcNow.AddHours(1)));
        }

        [Fact]
        public void Should_ThrowDomainException_When_CancellationTimeIsBeforeActivationTime()
        {
            License license = new License(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow);

            Assert.Throws<DomainException>(() => license.SetCancelled(DateTime.UtcNow.AddHours(-1)));
        }

        [Fact]
        public void Should_BeActivated_When_NoCancellationTimeIsSet()
        {
            License license = new License(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow);

            Assert.Equal(LicenseStatus.Activated, license.Status);
        }

        [Fact]
        public void Should_BeCancelled_When_ActivatedIsSetAfterCancellation()
        {
            License license = new License(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow);

            license.SetCancelled(DateTime.UtcNow);
            license.SetActivated(DateTime.UtcNow.AddHours(-1));

            Assert.Equal(LicenseStatus.Cancelled, license.Status);
        }
    }
}
