﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace WD.API.Licenses.UnitTests.Utils
{
    public static class EFCoreUtils
    {
        /// <summary>
        /// Create an new <see cref="DbContextOptions"/> for testing.
        /// Inspired: https://www.thereformedprogrammer.net/using-in-memory-databases-for-unit-testing-ef-core-applications/
        /// </summary>
        /// <typeparam name="TContext">DbContext to create options for.</typeparam>
        /// <returns>DbContext options.</returns>
        public static DbContextOptions<TContext> CreateOptions<TContext>()
            where TContext : DbContext
        {
            //This creates the SQLite connection string to in-memory database
            var connectionStringBuilder = new SqliteConnectionStringBuilder
            { DataSource = ":memory:" };
            var connectionString = connectionStringBuilder.ToString();

            //This creates a SqliteConnectionwith that string
            var connection = new SqliteConnection(connectionString);

            //The connection MUST be opened here
            connection.Open();

            //Now we have the EF Core commands to create SQLite options
            var builder = new DbContextOptionsBuilder<TContext>();
            builder.UseSqlite(connection);

            return builder.Options;
        }
    }
}
