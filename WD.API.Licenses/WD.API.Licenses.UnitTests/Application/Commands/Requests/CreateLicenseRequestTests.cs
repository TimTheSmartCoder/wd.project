﻿using System;
using WD.API.Licenses.Application.Commands.Requests;
using Xunit;

namespace WD.API.Licenses.UnitTests.Application.Commands.Requests
{
    public class CreateLicenseRequestTests
    {
        [Fact]
        public void Should_ThrowArgumentNullException_When_ModelIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new CreateLicenseRequest(null));
        }
    }
}
