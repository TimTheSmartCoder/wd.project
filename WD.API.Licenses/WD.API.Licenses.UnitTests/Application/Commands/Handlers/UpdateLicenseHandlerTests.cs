﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Application.Commands.Handlers;
using WD.API.Licenses.Application.Commands.Models;
using WD.API.Licenses.Application.Commands.Requests;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.Core;
using WD.API.Licenses.Domain.UnitOfWorks;
using WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Licenses.UnitTests.Utils;
using Xunit;

namespace WD.API.Licenses.UnitTests.Application.Commands.Handlers
{
    public class UpdateLicenseHandlerTests
    {
        [Theory]
        [InlineData(-1, 1, 1)]
        [InlineData(int.MinValue, 1, 1)]
        [InlineData(1, -1, 1)]
        [InlineData(1, int.MinValue, 1)]
        [InlineData(1, 1, -1)]
        [InlineData(1, 1, int.MinValue)]
        public async Task Should_ThrowValidationException_When_ParametersIsInvalid(int id, int customer, int user)
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            using (var dbContext = new LicensesDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new LicensesDbContext(options))
            {
                var request = CreateDefaultRequest(id, customer, user, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<ValidationException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_UnitOfWorkIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new UpdateLicenseHandler(null, CreateDefaultLogger()));
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_LoggerIsNull()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            using (var dbContext = new LicensesDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new LicensesDbContext(options))
            {

                Assert.Throws<ArgumentNullException>(() => new UpdateLicenseHandler(CreateDefaultUnitOfWork(dbContext), null));
            }
        }

        [Fact]
        public async Task Should_ThrowBadRequestException_When_LicenseDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            using (var dbContext = new LicensesDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new LicensesDbContext(options))
            {
                var request = CreateDefaultRequest(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);


                await Assert.ThrowsAsync<BadRequestException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public async Task Should_BeSuccessfull_When_LicenseDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            var information = new { Id = 1, Customer = 1, User = 1, Type = LicenseType.Mobile, Activated = DateTime.UtcNow };

            using (var dbContext = new LicensesDbContext(options))
            {
                dbContext.Database.EnsureCreated();

                dbContext.Set<License>().Add(new License(
                    information.Id, 
                    information.Customer, 
                    information.User, 
                    LicenseType.Mobile, 
                    information.Activated,
                    DateTime.UtcNow));

                dbContext.SaveChanges();
            }

            using (var dbContext = new LicensesDbContext(options))
            {
                var request = CreateDefaultRequest(1, 2, 2, LicenseType.Pro, DateTime.UtcNow.AddHours(-1));
                var handler = CreateDefaultHandler(dbContext);

                await handler.Handle(request, new CancellationTokenSource().Token);

                var license = dbContext.Set<License>().First(x => x.Id == 1);

                Assert.NotEqual(information.Customer, license.Customer);
                Assert.NotEqual(information.User, license.User);
                Assert.NotEqual(information.Type, license.Type);
                Assert.NotEqual(information.Activated, license.Activated);
            }
        }

        public static UpdateLicenseRequest CreateDefaultRequest(int id, int customer, int user, LicenseType type, DateTime activated)
        {
            UpdateLicenseModel model = new UpdateLicenseModel()
            {
                Id = id,
                Customer = customer,
                User = user,
                Activated = activated,
                Type = type
            };

            return new UpdateLicenseRequest(model);
        }

        public static UpdateLicenseHandler CreateDefaultHandler(LicensesDbContext licensesDbContext)
        {
            return new UpdateLicenseHandler(
                CreateDefaultUnitOfWork(licensesDbContext),
                CreateDefaultLogger());
        }

        public static IUnitOfWork CreateDefaultUnitOfWork(LicensesDbContext licensesDbContext)
        {
            return new UnitOfWork(licensesDbContext);
        }

        public static ILogger<UpdateLicenseHandler> CreateDefaultLogger()
        {
            return new LoggerFactory().CreateLogger<UpdateLicenseHandler>();
        }
    }
}
