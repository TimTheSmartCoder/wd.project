﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Licenses.Application.Commands.Handlers;
using WD.API.Licenses.Application.Commands.Models;
using WD.API.Licenses.Application.Commands.Requests;
using WD.API.Licenses.Domain.Aggregates.LicenseAggregate;
using WD.API.Licenses.Domain.Core;
using WD.API.Licenses.Domain.UnitOfWorks;
using WD.API.Licenses.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Licenses.UnitTests.Utils;
using Xunit;

namespace WD.API.Licenses.UnitTests.Application.Commands.Handlers
{
    public class CreateLicenseHandlerTests
    {
        [Theory]
        [InlineData(-1, 1, 1)]
        [InlineData(int.MinValue, 1, 1)]
        [InlineData(1, -1, 1)]
        [InlineData(1, int.MinValue, 1)]
        [InlineData(1, 1, -1)]
        [InlineData(1, 1, int.MinValue)]
        public async Task Should_ThrowValidationException_When_ParametersIsInvalid(int id, int customer, int user)
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            using (var dbContext = new LicensesDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new LicensesDbContext(options))
            {
                var request = CreateDefaultRequest(id, customer, user, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<ValidationException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_UnitOfWorkIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new CreateLicenseHandler(null, CreateDefaultLogger()));
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_LoggerIsNull()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            using (var dbContext = new LicensesDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new LicensesDbContext(options))
            {

                Assert.Throws<ArgumentNullException>(() => new CreateLicenseHandler(CreateDefaultUnitOfWork(dbContext), null));
            }
        }

        [Fact]
        public async Task Should_ThrowBadRequestException_When_LicenseAlreadyExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            using (var dbContext = new LicensesDbContext(options))
            {
                dbContext.Database.EnsureCreated();

                dbContext.Set<License>().Add(new License(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow, DateTime.UtcNow));

                await dbContext.SaveChangesAsync();
            }

            using (var dbContext = new LicensesDbContext(options))
            {
                var request = CreateDefaultRequest(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);


                await Assert.ThrowsAsync<BadRequestException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public async Task Should_BeSuccessfull_When_LicenseDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<LicensesDbContext>();

            using (var dbContext = new LicensesDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new LicensesDbContext(options))
            {
                var request = CreateDefaultRequest(1, 1, 1, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await handler.Handle(request, new CancellationTokenSource().Token);

                Assert.Equal(1, dbContext.Set<License>().First(x => x.Id == 1).Id);
            }
        }

        public static CreateLicenseRequest CreateDefaultRequest(int id, int customer, int user, LicenseType type, DateTime activated)
        {
            CreateLicenseModel model = new CreateLicenseModel()
            {
                Id = id,
                Customer = customer,
                User = user,
                Activated = activated,
                Type = type
            };

            return new CreateLicenseRequest(model);
        }

        public static CreateLicenseHandler CreateDefaultHandler(LicensesDbContext licensesDbContext)
        {
            return new CreateLicenseHandler(
                CreateDefaultUnitOfWork(licensesDbContext),
                CreateDefaultLogger());
        }

        public static IUnitOfWork CreateDefaultUnitOfWork(LicensesDbContext licensesDbContext)
        {
            return new UnitOfWork(licensesDbContext);
        }

        public static ILogger<CreateLicenseHandler> CreateDefaultLogger()
        {
            return new LoggerFactory().CreateLogger<CreateLicenseHandler>();
        }
    }
}
