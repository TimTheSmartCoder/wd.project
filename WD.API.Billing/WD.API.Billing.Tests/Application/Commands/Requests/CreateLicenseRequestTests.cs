﻿using System;
using WD.API.Billing.Application.Commands.Requests;
using Xunit;

namespace WD.API.Billing.Tests.Application.Commands.Requests
{
    public class CreateLicenseRequestTests
    {
        [Fact]
        public void Should_ThrowArgumentNullException_When_ModelIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new CreateLicenseRequest(null));
        }
    }
}
