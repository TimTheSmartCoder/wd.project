﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Handlers;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Billing.Tests.Utils;
using Xunit;
using static WD.API.Billing.Domain.Aggregates.LicenseAggregate.License;

namespace WD.API.Billing.Tests.Application.Commands.Handlers
{
    public class CreateLicenseHandlerTests
    {
        [Theory]
        [InlineData(-1, 1)]
        [InlineData(int.MinValue, 1)]
        [InlineData(1, -1)]
        [InlineData(1, int.MinValue)]
        public async Task Should_ThrowValidationException_When_ParametersIsInvalid(int id, int customer)
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(id, customer, 1, DateTime.UtcNow, DateTime.UtcNow, LicenseStatus.Activated, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<ValidationException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_UnitOfWorkIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new CreateLicenseHandler(null, CreateDefaultLogger()));
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_LoggerIsNull()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {

                Assert.Throws<ArgumentNullException>(() => new CreateLicenseHandler(CreateDefaultUnitOfWork(dbContext), null));
            }
        }

        [Fact]
        public async Task Should_ThrowBadRequestException_When_LicenseAlreadyExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
            {
                dbContext.Database.EnsureCreated();

                dbContext.Set<License>().Add(new License(1, 1, 1, DateTime.UtcNow, null, LicenseStatus.Activated, LicenseType.Mobile, DateTime.UtcNow));

                await dbContext.SaveChangesAsync();
            }

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1, 1, 1, DateTime.UtcNow, null, LicenseStatus.Activated, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);


                await Assert.ThrowsAsync<BadRequestException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public async Task Should_BeSuccessfull_When_LicenseDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1, 1, 1, DateTime.UtcNow, null, LicenseStatus.Activated, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await handler.Handle(request, new CancellationTokenSource().Token);

                Assert.Equal(1, dbContext.Set<License>().First(x => x.Id == 1).Id);
            }
        }


        public static CreateLicenseRequest CreateDefaultRequest(int id, int customer, int user, DateTime activation, DateTime? cancellation, LicenseStatus status, LicenseType type, DateTime modified)
        {
            CreateLicenseModel model = new CreateLicenseModel()
            {
                Id = id,
                Customer = customer,
                User = user,
                Activated = activation,
                Cancelled = cancellation,
                Status = status,
                Type = type,
                Modified = modified
            };

            return new CreateLicenseRequest(model);
        }

        public static CreateLicenseHandler CreateDefaultHandler(BillingDbContext billingDbContext)
        {
            return new CreateLicenseHandler(
                CreateDefaultUnitOfWork(billingDbContext),
                CreateDefaultLogger());
        }

        public static IUnitOfWork CreateDefaultUnitOfWork(BillingDbContext billingDbContext)
        {
            return new UnitOfWork(billingDbContext);
        }

        public static ILogger<CreateLicenseHandler> CreateDefaultLogger()
        {
            return new LoggerFactory().CreateLogger<CreateLicenseHandler>();
        }
    }
}
