﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Handlers;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Billing.Tests.Utils;
using Xunit;

namespace WD.API.Billing.Tests.Application.Commands.Handlers
{
    public class UpdateCustomerHandlerTests
    {
        [Theory]
        [InlineData(-1, "Test")]
        [InlineData(int.MinValue, "Test")]
        [InlineData(1, null)]
        public async Task Should_ThrowValidationException_When_ParametersIsInvalid(int id, string name)
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(id, name, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<ValidationException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_UnitOfWorkIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new UpdateCustomerHandler(null, CreateDefaultLogger()));
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_LoggerIsNull()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                Assert.Throws<ArgumentNullException>(() => new UpdateCustomerHandler(CreateDefaultUnitOfWork(dbContext), null));
            }
        }

        [Fact]
        public async Task Should_ThrowBadRequest_When_CustomerDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();
            
            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1, "Test", DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<BadRequestException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public async Task Should_BeSuccessfull_When_CustomerExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
            {
                dbContext.Database.EnsureCreated();

                dbContext.Set<Customer>().Add(new Customer(1, "Customer", DateTime.UtcNow));

                await dbContext.SaveChangesAsync();
            }
            
            using (var dbContext = new BillingDbContext(options))
            {
                var information = new { Name = "Customer Updated", Modified = DateTime.UtcNow };

                var request = CreateDefaultRequest(1, information.Name, information.Modified);
                var handler = CreateDefaultHandler(dbContext);

                await handler.Handle(request, new CancellationTokenSource().Token);

                Customer customer = dbContext.Set<Customer>().First(x => x.Id == 1);

                Assert.Equal(information.Name, customer.Name);
                Assert.Equal(information.Modified, customer.Modified);
            }
        }

        public static UpdateCustomerRequest CreateDefaultRequest(int id, string name, DateTime modified)
        {
            UpdateCustomerModel model = new UpdateCustomerModel()
            {
                Id = id,
                Name = name,
                Modified = modified
            };

            return new UpdateCustomerRequest(model);
        }

        public static UpdateCustomerHandler CreateDefaultHandler(BillingDbContext billingDbContext)
        {
            return new UpdateCustomerHandler(
                CreateDefaultUnitOfWork(billingDbContext),
                CreateDefaultLogger());
        }

        public static IUnitOfWork CreateDefaultUnitOfWork(BillingDbContext billingDbContext)
        {
            return new UnitOfWork(billingDbContext);
        }

        public static ILogger<UpdateCustomerHandler> CreateDefaultLogger()
        {
            return new LoggerFactory().CreateLogger<UpdateCustomerHandler>();
        }
    }
}
