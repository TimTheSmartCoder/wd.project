﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Handlers;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Billing.Tests.Utils;
using Xunit;

namespace WD.API.Billing.Tests.Application.Commands.Handlers
{
    public class DeleteCustomerHandlerTests
    {
        [Theory]
        [InlineData(-1)]
        [InlineData(int.MinValue)]
        public async Task Should_ThrowValidationException_When_ParametersIsInvalid(int id)
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(id);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<ValidationException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_UnitOfWorkIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new DeleteCustomerHandler(null, CreateDefaultLogger()));
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_LoggerIsNull()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                Assert.Throws<ArgumentNullException>(() => new DeleteCustomerHandler(CreateDefaultUnitOfWork(dbContext), null));
            }
        }

        [Fact]
        public async Task Should_ThrowBadRequest_When_CustomerDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<BadRequestException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public async Task Should_BeSuccessfull_When_CustomerExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
            {
                dbContext.Database.EnsureCreated();

                dbContext.Set<Customer>().Add(new Customer(1, "Customer", DateTime.UtcNow));

                await dbContext.SaveChangesAsync();
            }

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1);
                var handler = CreateDefaultHandler(dbContext);

                await handler.Handle(request, new CancellationTokenSource().Token);

                Customer customer = dbContext.Set<Customer>().FirstOrDefault(x => x.Id == 1);

                Assert.Null(customer);
            }
        }

        public static DeleteCustomerRequest CreateDefaultRequest(int id)
        {
            DeleteCustomerModel model = new DeleteCustomerModel()
            {
                Id = id,
            };

            return new DeleteCustomerRequest(model);
        }

        public static DeleteCustomerHandler CreateDefaultHandler(BillingDbContext billingDbContext)
        {
            return new DeleteCustomerHandler(
                CreateDefaultUnitOfWork(billingDbContext),
                CreateDefaultLogger());
        }

        public static IUnitOfWork CreateDefaultUnitOfWork(BillingDbContext billingDbContext)
        {
            return new UnitOfWork(billingDbContext);
        }

        public static ILogger<DeleteCustomerHandler> CreateDefaultLogger()
        {
            return new LoggerFactory().CreateLogger<DeleteCustomerHandler>();
        }
    }
}
