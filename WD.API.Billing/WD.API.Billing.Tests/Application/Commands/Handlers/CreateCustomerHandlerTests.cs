﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Handlers;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Billing.Tests.Utils;
using Xunit;

namespace WD.API.Billing.Tests.Application.Commands.Handlers
{
    public class CreateCustomerHandlerTests
    {
        [Theory]
        [InlineData(-1, "Test")]
        [InlineData(int.MinValue, "Test")]
        [InlineData(1, null)]
        public async Task Should_ThrowValidationException_When_ParametersIsInvalid(int id, string name)
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(id, name, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<ValidationException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_UnitOfWorkIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new CreateCustomerHandler(null, CreateDefaultLogger()));
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_LoggerIsNull()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                Assert.Throws<ArgumentNullException>(() => new CreateCustomerHandler(CreateDefaultUnitOfWork(dbContext), null));
            }
        }

        [Fact]
        public async Task Should_ThrowBadRequestException_When_CustomerAlreadyExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
            {
                dbContext.Database.EnsureCreated();

                dbContext.Set<Customer>().Add(new Customer(1, "Test", DateTime.UtcNow));

                await dbContext.SaveChangesAsync();
            }

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1, "Test", DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<BadRequestException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public async Task Should_BeSuccessfull_When_CustomerDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1, "Test", DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await handler.Handle(request, new CancellationTokenSource().Token);

                Assert.Equal(1, dbContext.Set<Customer>().First(x => x.Id == 1).Id);
            }
        }

        public static CreateCustomerRequest CreateDefaultRequest(int id, string name, DateTime modified)
        {
            CreateCustomerModel model = new CreateCustomerModel()
            {
                Id = id,
                Name = name,
                Modified = modified
            };

            return new CreateCustomerRequest(model);
        }

        public static CreateCustomerHandler CreateDefaultHandler(BillingDbContext billingDbContext)
        {
            return new CreateCustomerHandler(
                CreateDefaultUnitOfWork(billingDbContext),
                CreateDefaultLogger());
        }

        public static IUnitOfWork CreateDefaultUnitOfWork(BillingDbContext billingDbContext)
        {
            return new UnitOfWork(billingDbContext);
        }

        public static ILogger<CreateCustomerHandler> CreateDefaultLogger()
        {
            return new LoggerFactory().CreateLogger<CreateCustomerHandler>();
        }
    }
}
