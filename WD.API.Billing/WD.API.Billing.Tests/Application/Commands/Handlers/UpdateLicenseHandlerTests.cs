﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Handlers;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Billing.Tests.Utils;
using Xunit;
using static WD.API.Billing.Domain.Aggregates.LicenseAggregate.License;

namespace WD.API.Billing.Tests.Application.Commands.Handlers
{
    public class UpdateLicenseHandlerTests
    {
        [Theory]
        [InlineData(-1, 1, 1)]
        [InlineData(int.MinValue, 1, 1)]
        [InlineData(1, -1, 1)]
        [InlineData(1, int.MinValue, 1)]
        public async Task Should_ThrowValidationException_When_ParametersIsInvalid(int id, int customer, int user)
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(id, customer, user, DateTime.UtcNow, DateTime.UtcNow, LicenseStatus.Cancelled, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<ValidationException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_UnitOfWorkIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new UpdateLicenseHandler(null, CreateDefaultLogger()));
        }

        [Fact]
        public void Should_ThrowArgumentNullException_When_LoggerIsNull()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {

                Assert.Throws<ArgumentNullException>(() => new UpdateLicenseHandler(CreateDefaultUnitOfWork(dbContext), null));
            }
        }

        [Fact]
        public async Task Should_ThrowBadRequest_When_LicenseDoNotExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
                dbContext.Database.EnsureCreated();

            using (var dbContext = new BillingDbContext(options))
            {
                var request = CreateDefaultRequest(1, 1, 1, DateTime.UtcNow, null, LicenseStatus.Activated, LicenseType.Mobile, DateTime.UtcNow);
                var handler = CreateDefaultHandler(dbContext);

                await Assert.ThrowsAsync<BadRequestException>(() => handler.Handle(request, new CancellationTokenSource().Token));
            }
        }

        [Fact]
        public async Task Should_BeSuccessfull_When_CustomerExist()
        {
            // Options for database context.
            var options = EFCoreUtils.CreateOptions<BillingDbContext>();

            using (var dbContext = new BillingDbContext(options))
            {
                dbContext.Database.EnsureCreated();

                dbContext.Set<License>().Add(new License(1, 1,1, DateTime.UtcNow, null, LicenseStatus.Activated,LicenseType.Mobile, DateTime.UtcNow));

                await dbContext.SaveChangesAsync();
            }

            using (var dbContext = new BillingDbContext(options))
            {
                var information = new { Customer = 2, User = 2, Activated = DateTime.UtcNow, Cancelled = DateTime.UtcNow, Status = LicenseStatus.Cancelled, Type = LicenseType.Mobile, Modified = DateTime.UtcNow };

                var request = CreateDefaultRequest(1, information.Customer, information.User, information.Activated, information.Cancelled, information.Status, information.Type, information.Modified);
                var handler = CreateDefaultHandler(dbContext);

                await handler.Handle(request, new CancellationTokenSource().Token);

                License license = dbContext.Set<License>().First(x => x.Id == 1);

                Assert.Equal(information.Customer, license.Customer);
                Assert.Equal(information.User, license.User);
                Assert.Equal(information.Activated, license.Activated);
                Assert.Equal(information.Cancelled, license.Cancelled);
                Assert.Equal(information.Status, license.Status);
                Assert.Equal(information.Type, license.Type);
                Assert.Equal(information.Modified, license.Modified);
            }
        }

        public static UpdateLicenseRequest CreateDefaultRequest(int id, int customer, int user, DateTime activation, DateTime? cancellation, LicenseStatus status, LicenseType type, DateTime modified)
        {
            UpdateLicenseModel model = new UpdateLicenseModel()
            {
                Id = id,
                Customer = customer,
                User = user,
                Activated = activation,
                Cancelled = cancellation,
                Status = status,
                Type = type,
                Modified = modified
            };

            return new UpdateLicenseRequest(model);
        }

        public static UpdateLicenseHandler CreateDefaultHandler(BillingDbContext billingDbContext)
        {
            return new UpdateLicenseHandler(
                CreateDefaultUnitOfWork(billingDbContext),
                CreateDefaultLogger());
        }

        public static IUnitOfWork CreateDefaultUnitOfWork(BillingDbContext billingDbContext)
        {
            return new UnitOfWork(billingDbContext);
        }

        public static ILogger<UpdateLicenseHandler> CreateDefaultLogger()
        {
            return new LoggerFactory().CreateLogger<UpdateLicenseHandler>();
        }
    }
}
