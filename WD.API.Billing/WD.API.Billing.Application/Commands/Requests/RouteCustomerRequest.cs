﻿using MediatR;
using System;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Results;

namespace WD.API.Billing.Application.Commands.Requests
{
    public class RouteCustomerRequest
        : IRequest<RouteCustomerResult>
    {
        /// <summary>
        /// Construct's <see cref="RouteCustomerRequest"/> with the given information.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        public RouteCustomerRequest(RouteCustomerModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            Model = model;
        }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public RouteCustomerModel Model { get; }
    }
}
