﻿using MediatR;
using System;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Results;

namespace WD.API.Billing.Application.Commands.Requests
{
    public class DeleteCustomerRequest
        : IRequest<DeleteCustomerResult>
    {
        /// <summary>
        /// Construct's <see cref="DeleteCustomerRequest"/> with the given information.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        public DeleteCustomerRequest(DeleteCustomerModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            Model = model;
        }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public DeleteCustomerModel Model { get; }
    }
}
