﻿using MediatR;
using System;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Results;

namespace WD.API.Billing.Application.Commands.Requests
{
    public class CreateLicenseRequest
        : IRequest<CreateLicenseResult>
    {
        /// <summary>
        /// Construct's <see cref="CreateCustomerRequest"/> with the given information.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        public CreateLicenseRequest(CreateLicenseModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            Model = model;
        }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public CreateLicenseModel Model { get; }
    }
}
