﻿using MediatR;
using System;
using WD.API.Billing.Application.Commands.Results;

namespace WD.API.Billing.Application.Commands.Requests
{
    public class RouteLicenseRequest
        : IRequest<RouteLicenseResult>
    {
        /// <summary>
        /// Construct's <see cref="RouteLicenseRequest"/> with the given information.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        public RouteLicenseRequest(RouteLicenseModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            Model = model;
        }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public RouteLicenseModel Model { get; }
    }
}
