﻿using System;
using static WD.API.Billing.Domain.Aggregates.LicenseAggregate.License;

namespace WD.API.Billing.Application.Commands.Requests
{
    public class RouteLicenseModel
    {
        /// <summary>
        /// Id of the <see cref="License"/>.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id of the Customer who owns the license.
        /// </summary>
        public int Customer { get; set; }

        /// <summary>
        /// Id of the user, which are connected to the license.
        /// </summary>
        public int User { get; set; }

        /// <summary>
        /// <see cref="DateTime"/> of the activation in UTC.
        /// </summary>
        public DateTime Activated { get; set; }

        /// <summary>
        /// <see cref="DateTime"/> of cancellation in UTC.
        /// </summary>
        public DateTime? Cancelled { get; set; }

        /// <summary>
        /// <see cref="LicenseStatus"/> of the <see cref="License"/>.
        /// </summary>
        public LicenseStatus Status { get; set; }

        /// <summary>
        /// <see cref="LicenseType"/> of the <see cref="License"/>.
        /// </summary>
        public LicenseType Type { get; set; }

        /// <summary>
        /// UTC time of when the <see cref="License"/> were last modified.
        /// </summary>
        public DateTime Modified { get; set; }
    }
}
