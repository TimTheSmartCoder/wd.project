﻿using FluentValidation;
using WD.API.Billing.Application.Commands.Requests;

namespace WD.API.Billing.Application.Commands.Validators
{
    public class UpdateCustomerRequestValidator
        : AbstractValidator<UpdateCustomerRequest>
    {
        public UpdateCustomerRequestValidator()
        {
            RuleFor(x => x.Model)
                .NotNull()
                .WithMessage("Model is not supplied, but it is required.");

            When(x => x.Model != null, () => {

                RuleFor(x => x.Model.Id)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Id can't be less than zero.");

                RuleFor(x => x.Model.Modified)
                    .NotNull()
                    .WithMessage("Modified is not supplied, but it is required.");

                RuleFor(x => x.Model.Name)
                    .NotNull()
                    .WithMessage("Name is not supplied, but it is required.");
            });
        }
    }
}
