﻿using FluentValidation;
using WD.API.Billing.Application.Commands.Requests;

namespace WD.API.Billing.Application.Commands.Validators
{
    public class DeleteCustomerRequestValidator
        : AbstractValidator<DeleteCustomerRequest>
    {
        public DeleteCustomerRequestValidator()
        {
            RuleFor(x => x.Model)
                .NotNull()
                .WithMessage("Model is not supplied, but it is required.");

            When(x => x.Model != null, () => {

                RuleFor(x => x.Model.Id)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Id can't be less than zero.");
            });
        }
    }
}
