﻿using FluentValidation;
using WD.API.Billing.Application.Commands.Requests;

namespace WD.API.Billing.Application.Commands.Validators
{
    public class UpdateLicenseRequestValidator
        : AbstractValidator<UpdateLicenseRequest>
    {
        public UpdateLicenseRequestValidator()
        {
            RuleFor(x => x.Model)
                .NotNull()
                .WithMessage("Model is not supplied, but it is required.");

            When(x => x.Model != null, () => {

                RuleFor(x => x.Model.Id)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Id can't be less than zero.");

                RuleFor(x => x.Model.Customer)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Customer can't be less than zero.");
            });
        }
    }
}
