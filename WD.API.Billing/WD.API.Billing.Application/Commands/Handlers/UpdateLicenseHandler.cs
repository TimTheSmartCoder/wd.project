﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Application.Commands.Results;
using WD.API.Billing.Application.Commands.Validators;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;

namespace WD.API.Billing.Application.Commands.Handlers
{
    public class UpdateLicenseHandler
        : IRequestHandler<UpdateLicenseRequest, UpdateLicenseResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<UpdateLicenseHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<UpdateLicenseRequest> _validator;

        public UpdateLicenseHandler(IUnitOfWork unitOfWork, ILogger<UpdateLicenseHandler> logger)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _unitOfWork = unitOfWork;
            _logger = logger;

            _validator = new UpdateLicenseRequestValidator();
        }

        public async Task<UpdateLicenseResult> Handle(UpdateLicenseRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starts processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validate request successfully.");

            // Try to get license.
            License license = await _unitOfWork
                .Repositories
                .Licenses
                .FindAsync(request.Model.Id);

            if (license == null)
            {
                _logger.LogWarning("License do not exist with id: '{id}'.", request.Model.Id);

                throw new BadRequestException($"License do not exist with id: '{request.Model.Id}'.");
            }

            _logger.LogInformation("License were found with id: '{id}'.", request.Model.Id);

            _logger.LogInformation("Updating license with id: '{id}'.", request.Model.Id);

            // Update the license.
            license.SetUser(request.Model.User);
            license.SetType(request.Model.Type);
            license.SetCustomer(request.Model.Customer);
            license.SetActivated(request.Model.Activated);
            license.SetCancelled(request.Model.Cancelled);
            license.SetStatus(request.Model.Status);
            license.SetModified(request.Model.Modified);

            // Update license in repository.
            _unitOfWork.Repositories
                .Licenses
                .Update(license);

            // Save all changes.
            await _unitOfWork.SaveChanges(cancellationToken);

            _logger.LogInformation("Finished request successfully.");

            return new UpdateLicenseResult();
        }
    }
}
