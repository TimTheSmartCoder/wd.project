﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Application.Commands.Results;
using WD.API.Billing.Application.Commands.Validators;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;
using WD.API.Billing.Domain.UnitOfWorks;

namespace WD.API.Billing.Application.Commands.Handlers
{
    public class RouteLicenseHandler
        : IRequestHandler<RouteLicenseRequest, RouteLicenseResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<RouteLicenseHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<RouteLicenseRequest> _validator;

        /// <summary>
        /// <see cref="IMediator"/> to use.
        /// </summary>
        private readonly IMediator _mediator;

        public RouteLicenseHandler(IUnitOfWork unitOfWork, ILogger<RouteLicenseHandler> logger, IMediator mediator)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
            if (mediator == null)
                throw new ArgumentNullException(nameof(mediator));

            _unitOfWork = unitOfWork;
            _logger = logger;
            _mediator = mediator;

            _validator = new RouteLicenseRequestValidator();
        }

        public async Task<RouteLicenseResult> Handle(RouteLicenseRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starts processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validated request successfully.");

            _logger.LogInformation("Starts checking if license exist with id: '{id}'.", request.Model.Id);

            // Try to get license.
            License license = await _unitOfWork
                .Repositories
                .Licenses
                .FindAsync(request.Model.Id);

            if (license == null)
            {
                _logger.LogInformation("License do not exist with id: '{id}'.", request.Model.Id);

                // Prepare model for license creation.
                CreateLicenseModel model = new CreateLicenseModel()
                {
                    Id = request.Model.Id,
                    User = request.Model.User,
                    Activated = request.Model.Activated,
                    Cancelled = request.Model.Cancelled,
                    Customer = request.Model.Customer,
                    Modified = request.Model.Modified,
                    Type = request.Model.Type,
                    Status = request.Model.Status
                };

                _logger.LogInformation("Requesting creation of license with id: '{Id}'.", request.Model.Id);

                // Create request of the license.
                CreateLicenseResult result = await _mediator.Send(new CreateLicenseRequest(model));
            }
            else
            {
                _logger.LogInformation("License already exist with id: '{id}'.", request.Model.Id);

                if (license.Modified < request.Model.Modified)
                {
                    _logger.LogInformation("Updating license with id: '{Id}', Modifed in database: '{old}', Modified in model: 'new'.", 
                        request.Model.Id, license.Modified, request.Model.Modified);

                    // Prepare model for license update.
                    UpdateLicenseModel model = new UpdateLicenseModel()
                    {
                        Id = request.Model.Id,
                        Activated = request.Model.Activated,
                        Cancelled = request.Model.Cancelled,
                        Customer = request.Model.Customer,
                        Modified = request.Model.Modified,
                        Status = request.Model.Status,
                        Type = request.Model.Type,
                        User = request.Model.User
                    };

                    _logger.LogInformation("Request updating of license with id: '{Id}'.", request.Model.Id);

                    // Request update of the license.
                    UpdateLicenseResult result = await _mediator.Send(new UpdateLicenseRequest(model));
                }
                else
                {
                    _logger.LogInformation("Skipping update of license with id: '{Id}', Modifed in database: '{old}', Modified in model: 'new'.",
                        request.Model.Id, license.Modified, request.Model.Modified);
                }
            }

            _logger.LogInformation("Finished request successfully.");

            return new RouteLicenseResult();
        }
    }
}
