﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Application.Commands.Results;
using WD.API.Billing.Application.Commands.Validators;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;

namespace WD.API.Billing.Application.Commands.Handlers
{
    public class CreateLicenseHandler
        : IRequestHandler<CreateLicenseRequest, CreateLicenseResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<CreateLicenseHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<CreateLicenseRequest> _validator;

        public CreateLicenseHandler(IUnitOfWork unitOfWork, ILogger<CreateLicenseHandler> logger)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _unitOfWork = unitOfWork;
            _logger = logger;

            _validator = new CreateLicenseRequestValidator();
        }

        public async Task<CreateLicenseResult> Handle(CreateLicenseRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starts processing request.");

            // Validate the reuqest.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validated request successfully.");

            _logger.LogInformation("Starts checking if license exist with id: '{id}'.", request.Model.Id);

            // Try to get license.
            License license = await _unitOfWork
                .Repositories
                .Licenses
                .FindAsync(request.Model.Id);

            if (license != null)
            {
                _logger.LogWarning("License already exist with id: '{id}', skipping creation.", request.Model.Id);

                throw new BadRequestException($"License already exist with id: '{request.Model.Id}'.");
            }

            _logger.LogInformation("No license were found with id: '{id}'.", request.Model.Id);

            _logger.LogInformation("Creating license with id: '{id}'.");

            // Create license.
            license = new License(
                request.Model.Id,
                request.Model.Customer,
                request.Model.User,
                request.Model.Activated,
                request.Model.Cancelled,
                request.Model.Status,
                request.Model.Type,
                request.Model.Modified);

            // Add license to repositroy.
            _unitOfWork.Repositories
                .Licenses
                .Insert(license);

            // Save all changes.
            await _unitOfWork.SaveChanges(cancellationToken);

            _logger.LogInformation("Finished request successfully.");

            return new CreateLicenseResult();
        }
    }
}
