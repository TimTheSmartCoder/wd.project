﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Application.Commands.Results;
using WD.API.Billing.Application.Commands.Validators;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.UnitOfWorks;

namespace WD.API.Billing.Application.Commands.Handlers
{
    public class RouteCustomerHandler
        : IRequestHandler<RouteCustomerRequest, RouteCustomerResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<RouteCustomerHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<RouteCustomerRequest> _validator;

        /// <summary>
        /// <see cref="IMediator"/> to use.
        /// </summary>
        private readonly IMediator _mediator;

        public RouteCustomerHandler(IUnitOfWork unitOfWork, ILogger<RouteCustomerHandler> logger, IMediator mediator)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
            if (mediator == null)
                throw new ArgumentNullException(nameof(mediator));

            _unitOfWork = unitOfWork;
            _logger = logger;
            _mediator = mediator;

            _validator = new RouteCustomerRequestValidator();
        }

        public async Task<RouteCustomerResult> Handle(RouteCustomerRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starts processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validated request successfully.");

            _logger.LogInformation("Starts checking if license exist with id: '{id}'.", request.Model.Id);

            // Try to get customer.
            Customer customer = await _unitOfWork
                .Repositories
                .Customers
                .FindAsync(request.Model.Id);

            if (customer == null)
            {
                _logger.LogInformation("Customer do not exist with id: '{id}'.", request.Model.Id);

                // Prepare model for customer creation.
                CreateCustomerModel model = new CreateCustomerModel()
                {
                    Id = request.Model.Id,
                    Modified = request.Model.Modified,
                    Name = request.Model.Name
                };

                _logger.LogInformation("Requesting creation of customer with id: '{Id}'.", request.Model.Id);

                // Request creation of customer.
                CreateCustomerResult result = await _mediator.Send(new CreateCustomerRequest(model));
            }
            else
            {
                _logger.LogInformation("Customer already exist with id: '{id}'.", request.Model.Id);

                if (customer.Modified < request.Model.Modified)
                {
                    _logger.LogInformation("Updating customer with id: '{Id}', Modifed in database: '{old}', Modified in model: 'new'.",
                        request.Model.Id, customer.Modified, request.Model.Modified);

                    // Prepare model for update.
                    UpdateCustomerModel model = new UpdateCustomerModel()
                    {
                        Id = request.Model.Id,
                        Modified = request.Model.Modified,
                        Name = request.Model.Name
                    };

                    _logger.LogInformation("Request updating of customer with id: '{Id}'.", request.Model.Id);

                    // Request update of customer.
                    UpdateCustomerResult result = await _mediator.Send(new UpdateCustomerRequest(model));
                }
                else
                {
                    _logger.LogInformation("Skipping update of customer with id: '{Id}', Modifed in database: '{old}', Modified in model: 'new'.",
                        request.Model.Id, customer.Modified, request.Model.Modified);
                }
            }

            _logger.LogInformation("Finished request successfully.");

            return new RouteCustomerResult();
        }
    }
}
