﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Application.Commands.Results;
using WD.API.Billing.Application.Commands.Validators;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;

namespace WD.API.Billing.Application.Commands.Handlers
{
    public class DeleteCustomerHandler
        : IRequestHandler<DeleteCustomerRequest, DeleteCustomerResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<DeleteCustomerHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<DeleteCustomerRequest> _validator;

        public DeleteCustomerHandler(
            IUnitOfWork unitOfWork, ILogger<DeleteCustomerHandler> logger)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _unitOfWork = unitOfWork;
            _logger = logger;

            _validator = new DeleteCustomerRequestValidator();
        }

        public async Task<DeleteCustomerResult> Handle(DeleteCustomerRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starts processing request.");

            // Validate the requqest.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validate request successfully.");

            _logger.LogInformation("Starts checking if customer exist with id: '{id}'.", request.Model.Id);

            Customer customer = await _unitOfWork
                .Repositories
                .Customers
                .FindAsync(request.Model.Id);

            if (customer == null)
            {
                _logger.LogWarning("Customer do not exist with id: '{id}'.", request.Model.Id);

                throw new BadRequestException($"Customer do not exist with id: '{request.Model.Id}'.");
            }

            _logger.LogInformation("Customer were found with id: '{id}'.", request.Model.Id);

            _logger.LogInformation("Deleting customer with id: '{id}'.", request.Model.Id);

            // Delete customer.
            _unitOfWork.Repositories
                .Customers
                .Delete(customer);

            // Save all changes.
            await _unitOfWork.SaveChanges(cancellationToken);

            _logger.LogInformation("Finished request successfully.");

            return new DeleteCustomerResult();
        }
    }
}
