﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Application.Commands.Results;
using WD.API.Billing.Application.Commands.Validators;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;

namespace WD.API.Billing.Application.Commands.Handlers
{
    public class UpdateCustomerHandler
        : IRequestHandler<UpdateCustomerRequest, UpdateCustomerResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<UpdateCustomerHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<UpdateCustomerRequest> _validator;

        public UpdateCustomerHandler(
            IUnitOfWork unitOfWork, ILogger<UpdateCustomerHandler> logger)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _unitOfWork = unitOfWork;
            _logger = logger;

            _validator = new UpdateCustomerRequestValidator();
        }

        public async Task<UpdateCustomerResult> Handle(
            UpdateCustomerRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starts processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validate request successfully.");

            // Try to get customer.
            Customer customer = await _unitOfWork
                .Repositories
                .Customers
                .FindAsync(request.Model.Id);

            if (customer == null)
            {
                _logger.LogWarning("Customer do not exist with id: '{id}'.", request.Model.Id);

                throw new BadRequestException($"Customer do not exist with id: '{request.Model.Id}'.");
            }

            _logger.LogInformation("Customer were found with id: '{id}'.", request.Model.Id);

            _logger.LogInformation("Updating customer with id: '{id}'.", request.Model.Id);

            // Update properties on the customer.
            customer.SetName(request.Model.Name);
            customer.SetModified(request.Model.Modified);

            // Save all changes.
            await _unitOfWork.SaveChanges(cancellationToken);

            _logger.LogInformation("Finished request successfully.");

            return new UpdateCustomerResult();
        }
    }
}
