﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Application.Commands.Results;
using WD.API.Billing.Application.Commands.Validators;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.Core;
using WD.API.Billing.Domain.UnitOfWorks;

namespace WD.API.Billing.Application.Commands.Handlers
{
    public class CreateCustomerHandler
        : IRequestHandler<CreateCustomerRequest, CreateCustomerResult>
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> to use.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<CreateCustomerHandler> _logger;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<CreateCustomerRequest> _validator;

        public CreateCustomerHandler(
            IUnitOfWork unitOfWork, ILogger<CreateCustomerHandler> logger)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _unitOfWork = unitOfWork;
            _logger = logger;

            _validator = new CreateCustomerRequestValidator();
        }

        public async Task<CreateCustomerResult> Handle(CreateCustomerRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starts processing request.");

            // Validate the request.
            _validator.ValidateAndThrow(request);

            _logger.LogInformation("Validate request successfully.");

            _logger.LogInformation("Starts checking if customer exist with id: '{id}'.", request.Model.Id);

            // Get customer, if already exist.
            Customer customer = await _unitOfWork
                .Repositories
                .Customers
                .FindAsync(request.Model.Id);

            if (customer != null)
            {
                _logger.LogWarning("Customer already exist with id: '{id}', skipping creation.", request.Model.Id);

                throw new BadRequestException($"Customer already exist with id: '{request.Model.Id}', skipping creation.");
            }

            _logger.LogInformation("No customer were found with id: '{id}'.", request.Model.Id);

            _logger.LogInformation("Creating customer with id: '{id}'.");

            // Create new customer.
            customer = new Customer(
                request.Model.Id, 
                request.Model.Name, 
                request.Model.Modified);

            // Add new customer to repository.
            _unitOfWork.Repositories
                .Customers
                .Insert(customer);

            // Save all changes.
            await _unitOfWork.SaveChanges(cancellationToken);

            _logger.LogInformation("Finished request successfully.");

            return new CreateCustomerResult();
        }
    }
}
