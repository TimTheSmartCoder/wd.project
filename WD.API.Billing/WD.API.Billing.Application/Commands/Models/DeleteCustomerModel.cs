﻿using System;

namespace WD.API.Billing.Application.Commands.Models
{
    public class DeleteCustomerModel
    {
        /// <summary>
        /// Id of the customer.
        /// </summary>
        public int Id { get; set; }
    }
}
