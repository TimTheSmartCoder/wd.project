﻿using System;

namespace WD.API.Billing.Application.Commands.Models
{
    public class UpdateCustomerModel
    {
        /// <summary>
        /// Id of the customer.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the customer.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// UTC time of when the customer was modified.
        /// </summary>
        public DateTime Modified { get; set; }
    }
}
