﻿using System;
using System.Threading.Tasks;
using WD.API.Billing.IntegrationTests.BackgroundServices;

namespace WD.API.Billing.IntegrationTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Run(args).GetAwaiter().GetResult();
        }

        static async Task Run(string[] args)
        {
            // Connection string for Azure Service Bus to use under testing.
            string azureServiceBusConnectionString = "";

            // Database connection string to use under testing.
            string databaseConnectionString = "";

            // Create test.
            LicenseBackgroundServiceTests licenseBackgroundService
                = new LicenseBackgroundServiceTests(azureServiceBusConnectionString, databaseConnectionString);

            // Create test.
            CustomerBackgroundServiceTests customerBackgroundServiceTests
                = new CustomerBackgroundServiceTests(azureServiceBusConnectionString, databaseConnectionString);

            Console.WriteLine("Starting test of LicenseBackgroundService.");

            // Run test for license background service.
            await licenseBackgroundService.Run();

            Console.WriteLine();
            Console.WriteLine("Successfully tested LicenseBackgroundService.");

            Console.WriteLine("Starting test of CustomerBackgroundService.");

            // Run test for customer background service.
            await customerBackgroundServiceTests.Run();

            Console.WriteLine();
            Console.WriteLine("Successfully tested CustomerBackgroundService.");

            Console.WriteLine();
            Console.WriteLine("All test are done.");
            Console.ReadLine();
        }
    }
}
