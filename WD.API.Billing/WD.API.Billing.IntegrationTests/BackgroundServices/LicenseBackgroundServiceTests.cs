﻿using Microsoft.Azure.ServiceBus;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Requests;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using Xunit;
using static WD.API.Billing.Domain.Aggregates.LicenseAggregate.License;

namespace WD.API.Billing.IntegrationTests.BackgroundServices
{
    public class LicenseBackgroundServiceTests
    {
        /// <summary>
        /// Azure Service Bus connection string.
        /// </summary>
        private readonly string _azureServiceBusConnectionString;

        /// <summary>
        /// Database connection string.
        /// </summary>
        private readonly string _databaseConnectionString;

        public LicenseBackgroundServiceTests(string azureServiceBusConnectionString, string databaseConnectionString)
        {
            if (azureServiceBusConnectionString == null)
                throw new ArgumentNullException(nameof(azureServiceBusConnectionString));
            if (databaseConnectionString == null)
                throw new ArgumentNullException(nameof(databaseConnectionString));

            _azureServiceBusConnectionString = azureServiceBusConnectionString;
            _databaseConnectionString = databaseConnectionString;
        }

        public async Task Run()
        {
            Console.WriteLine();

            Console.Write($"Running: '{nameof(Should_AddLicense_When_LicenseAdd)}'. ");
            await Should_AddLicense_When_LicenseAdd();
            Console.WriteLine("Success.");

            Console.Write($"Running: '{nameof(Should_UpdateLicense_When_LicenseUpdate)}'. ");
            await Should_UpdateLicense_When_LicenseUpdate();
            Console.WriteLine("Success.");

            Console.Write($"Running: '{nameof(Should_AddLicense_When_LicenseUpdate)}'. ");
            await Should_AddLicense_When_LicenseUpdate();
            Console.WriteLine("Success.");
        }

        public async Task Should_AddLicense_When_LicenseAdd()
        {
            // Model of license.
            RouteLicenseModel model = new RouteLicenseModel()
            {
                Activated = DateTime.UtcNow,
                Cancelled = null,
                Customer = 1,
                Id = 1,
                Modified = DateTime.UtcNow,
                Status = LicenseStatus.Activated,
                Type = LicenseType.Mobile,
                User = 1
            };

            // Convert model to json.
            string modelAsJson = JsonConvert.SerializeObject(model);

            // Send message.
            await SendMessage(modelAsJson, "Created");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Unit of work connected to database.
            IUnitOfWork unitOfWork = GetUnitOfWork();

            // Get the license.
            var license = await unitOfWork.Repositories
                .Licenses
                .FindAsync(model.Id);

            // Check if the license exist.
            Assert.NotNull(license);

            // Check if model is created correctly.
            Assert.Equal(model.Activated, license.Activated);
            Assert.Equal(model.Cancelled, license.Cancelled);
            Assert.Equal(model.Customer, license.Customer);
            Assert.Equal(model.Id, license.Id);
            Assert.Equal(model.Modified, license.Modified);
            Assert.Equal(model.Type, license.Type);
            Assert.Equal(model.User, license.User);
            Assert.Equal(model.Status, license.Status);
        }
        
        public async Task Should_UpdateLicense_When_LicenseUpdate()
        {
            // Model of license.
            RouteLicenseModel createModel = new RouteLicenseModel()
            {
                Activated = DateTime.UtcNow,
                Cancelled = null,
                Customer = 2,
                Id = 2,
                Modified = DateTime.UtcNow,
                Status = LicenseStatus.Activated,
                Type = LicenseType.Mobile,
                User = 2
            };

            // Convert model to json.
            string modelAsJson = JsonConvert.SerializeObject(createModel);

            // Send message.
            await SendMessage(modelAsJson, "Created");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Model for update.
            RouteLicenseModel updateModel = new RouteLicenseModel()
            {
                Activated = DateTime.UtcNow,
                Cancelled = DateTime.UtcNow,
                Customer = createModel.Customer,
                Id = createModel.Id,
                Modified = DateTime.UtcNow,
                Status = LicenseStatus.Cancelled,
                Type = LicenseType.Mobile,
                User = createModel.User
            };

            // Convert model to json.
            modelAsJson = JsonConvert.SerializeObject(updateModel);

            // Send message.
            await SendMessage(modelAsJson, "Updated");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Unit of work connected to database.
            IUnitOfWork unitOfWork = GetUnitOfWork();

            // Get the license.
            var license = await unitOfWork.Repositories
                .Licenses
                .FindAsync(updateModel.Id);

            // Check if the license exist.
            Assert.NotNull(license);

            // Check if model is created correctly.
            Assert.Equal(updateModel.Activated, license.Activated);
            Assert.Equal(updateModel.Cancelled, license.Cancelled);
            Assert.Equal(updateModel.Customer, license.Customer);
            Assert.Equal(updateModel.Id, license.Id);
            Assert.Equal(updateModel.Modified, license.Modified);
            Assert.Equal(updateModel.Type, license.Type);
            Assert.Equal(updateModel.User, license.User);
            Assert.Equal(updateModel.Status, license.Status);
        }

        public async Task Should_AddLicense_When_LicenseUpdate()
        {
            // Model of license.
            RouteLicenseModel model = new RouteLicenseModel()
            {
                Activated = DateTime.UtcNow,
                Cancelled = null,
                Customer = 3,
                Id = 3,
                Modified = DateTime.UtcNow,
                Status = LicenseStatus.Activated,
                Type = LicenseType.Mobile,
                User = 3
            };

            // Convert model to json.
            string modelAsJson = JsonConvert.SerializeObject(model);

            // Send message.
            await SendMessage(modelAsJson, "Updated");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Unit of work connected to database.
            IUnitOfWork unitOfWork = GetUnitOfWork();

            // Get the license.
            var license = await unitOfWork.Repositories
                .Licenses
                .FindAsync(model.Id);

            // Check if the license exist.
            Assert.NotNull(license);

            // Check if model is created correctly.
            Assert.Equal(model.Activated, license.Activated);
            Assert.Equal(model.Cancelled, license.Cancelled);
            Assert.Equal(model.Customer, license.Customer);
            Assert.Equal(model.Id, license.Id);
            Assert.Equal(model.Modified, license.Modified);
            Assert.Equal(model.Type, license.Type);
            Assert.Equal(model.User, license.User);
            Assert.Equal(model.Status, license.Status);
        }

        private IUnitOfWork GetUnitOfWork()
        {
            // Builder for building db context.
            var builder = new DbContextOptionsBuilder<BillingDbContext>();

            // Database server to use in testing.
            builder.UseSqlServer(_databaseConnectionString);

            return new UnitOfWork(new BillingDbContext(builder.Options));
        }

        private async Task SendMessage(string body, string label)
        {
            ITopicClient topicClient = new TopicClient(
                _azureServiceBusConnectionString,
                "License");

            // Create message with body.
            Message message = new Message(Encoding.UTF8.GetBytes(body));

            // Add label.
            message.Label = label;

            // Add content type.
            message.ContentType = "application/json";

            // Add Correlation id.
            message.CorrelationId = Guid.NewGuid().ToString();

            // Send message and wait for result.
            await topicClient.SendAsync(message);

            // Close client.
            await topicClient.CloseAsync();
        }
    }
}
