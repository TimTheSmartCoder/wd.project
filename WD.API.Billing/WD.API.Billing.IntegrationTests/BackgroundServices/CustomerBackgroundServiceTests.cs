﻿using Microsoft.Azure.ServiceBus;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;
using WD.API.Billing.Application.Commands.Models;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using Xunit;

namespace WD.API.Billing.IntegrationTests.BackgroundServices
{
    public class CustomerBackgroundServiceTests
    {
        /// <summary>
        /// Azure Service Bus connection string.
        /// </summary>
        private readonly string _azureServiceBusConnectionString;

        /// <summary>
        /// Database connection string.
        /// </summary>
        private readonly string _databaseConnectionString;

        public CustomerBackgroundServiceTests(string azureServiceBusConnectionString, string databaseConnectionString)
        {
            if (azureServiceBusConnectionString == null)
                throw new ArgumentNullException(nameof(azureServiceBusConnectionString));
            if (databaseConnectionString == null)
                throw new ArgumentNullException(nameof(databaseConnectionString));

            _azureServiceBusConnectionString = azureServiceBusConnectionString;
            _databaseConnectionString = databaseConnectionString;
        }

        public async Task Run()
        {
            Console.WriteLine();

            Console.Write($"Running: '{nameof(Should_AddCustomer_When_CustomerAdded)}'. ");
            await Should_AddCustomer_When_CustomerAdded();
            Console.WriteLine("Success.");

            Console.Write($"Running: '{nameof(Should_AddCustomer_When_CustomerUpdate)}'. ");
            await Should_AddCustomer_When_CustomerUpdate();
            Console.WriteLine("Success.");

            Console.Write($"Running: '{nameof(Should_UpdateCustomer_When_CustomerUpdate)}'. ");
            await Should_UpdateCustomer_When_CustomerUpdate();
            Console.WriteLine("Success.");
        }

        public async Task Should_AddCustomer_When_CustomerAdded()
        {
            // Model of customer.
            RouteCustomerModel model = new RouteCustomerModel()
            {
                Id = 1,
                Modified = DateTime.UtcNow,
                Name = "Customer 1"
            };

            // Convert model to json.
            string modelAsJson = JsonConvert.SerializeObject(model);

            // Send message.
            await SendMessage(modelAsJson, "Created");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Unit of work connected to database.
            IUnitOfWork unitOfWork = GetUnitOfWork();

            // Get the license.
            var customer = await unitOfWork.Repositories
                .Customers
                .FindAsync(model.Id);

            // Check if the license exist.
            Assert.NotNull(customer);

            // Check if model is created correctly.
            Assert.Equal(model.Id, customer.Id);
            Assert.Equal(model.Modified, customer.Modified);
            Assert.Equal(model.Name, customer.Name);
        }

        public async Task Should_AddCustomer_When_CustomerUpdate()
        {
            // Model of customer.
            RouteCustomerModel model = new RouteCustomerModel()
            {
                Id = 2,
                Modified = DateTime.UtcNow,
                Name = "Customer 2"
            };

            // Convert model to json.
            string modelAsJson = JsonConvert.SerializeObject(model);

            // Send message.
            await SendMessage(modelAsJson, "Updated");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Unit of work connected to database.
            IUnitOfWork unitOfWork = GetUnitOfWork();

            // Get the license.
            var customer = await unitOfWork.Repositories
                .Customers
                .FindAsync(model.Id);

            // Check if the license exist.
            Assert.NotNull(customer);

            // Check if model is created correctly.
            Assert.Equal(model.Id, customer.Id);
            Assert.Equal(model.Modified, customer.Modified);
            Assert.Equal(model.Name, customer.Name);
        }

        public async Task Should_UpdateCustomer_When_CustomerUpdate()
        {
            // Model of customer.
            RouteCustomerModel model = new RouteCustomerModel()
            {
                Id = 3,
                Modified = DateTime.UtcNow,
                Name = "Customer 3"
            };

            // Convert model to json.
            string modelAsJson = JsonConvert.SerializeObject(model);

            // Send message.
            await SendMessage(modelAsJson, "Created");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Model of customer.
            model = new RouteCustomerModel()
            {
                Id = 3,
                Modified = DateTime.UtcNow,
                Name = "Customer 3 Updated"
            };

            // Convert model to json.
            modelAsJson = JsonConvert.SerializeObject(model);

            // Send message.
            await SendMessage(modelAsJson, "Updated");

            // Wait for a while, allow processing to happen.
            await Task.Delay(5000);

            // Unit of work connected to database.
            IUnitOfWork unitOfWork = GetUnitOfWork();

            // Get the license.
            var customer = await unitOfWork.Repositories
                .Customers
                .FindAsync(model.Id);

            // Check if the license exist.
            Assert.NotNull(customer);

            // Check if model is created correctly.
            Assert.Equal(model.Id, customer.Id);
            Assert.Equal(model.Modified, customer.Modified);
            Assert.Equal(model.Name, customer.Name);
        }

        private IUnitOfWork GetUnitOfWork()
        {
            // Builder for building db context.
            var builder = new DbContextOptionsBuilder<BillingDbContext>();

            // Database server to use in testing.
            builder.UseSqlServer(_databaseConnectionString);

            return new UnitOfWork(new BillingDbContext(builder.Options));
        }

        private async Task SendMessage(string body, string label)
        {
            ITopicClient topicClient = new TopicClient(
                _azureServiceBusConnectionString,
                "Customer");

            // Create message with body.
            Message message = new Message(Encoding.UTF8.GetBytes(body));

            // Add label.
            message.Label = label;

            // Add content type.
            message.ContentType = "application/json";

            // Add Correlation id.
            message.CorrelationId = Guid.NewGuid().ToString();

            // Send message and wait for result.
            await topicClient.SendAsync(message);

            // Close client.
            await topicClient.CloseAsync();
        }
    }
}
