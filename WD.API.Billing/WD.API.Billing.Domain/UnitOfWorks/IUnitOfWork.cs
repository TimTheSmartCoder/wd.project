﻿using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Domain.UnitOfWorks.Repositories;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;

namespace WD.API.Billing.Domain.UnitOfWorks
{
    /// <summary>
    /// <see cref="IUnitOfWork"/> to represent unit of work.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// List of repositories available in the <see cref="IUnitOfWork"/>.
        /// </summary>
        IRepositoryList Repositories { get; }

        /// <summary>
        /// Save all changes applied to the <see cref="IUnitOfWork"/>.
        /// </summary>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> for cancellation.</param>
        /// <returns>True if success, otherwise false.</returns>
        /// <exception cref="UnitOfWorkException">Throws exception if fail to save changes.</exception>
        Task<bool> SaveChanges(CancellationToken cancellationToken = default(CancellationToken));
    }

    /// <summary>
    /// List of <see cref="IRepository{TAggregate}"/> available in <see cref="IUnitOfWork"/>.
    /// </summary>
    public interface IRepositoryList
    {
        /// <summary>
        /// Repository for <see cref="Customer"/>.
        /// </summary>
        ICustomerRepository Customers { get; }

        /// <summary>
        /// Repository for <see cref="License"/>.
        /// </summary>
        ILicenseRepository Licenses { get; }
    }
}
