﻿using WD.API.Billing.Domain.Core;

namespace WD.API.Billing.Domain.UnitOfWorks.Repositories
{
    /// <summary>
    /// Inteface representing a <see cref="IRepository{TAggregate}"/>.
    /// </summary>
    /// <typeparam name="TAggregate">Aggregate the repository is for.</typeparam>
    public interface IRepository<TAggregate>
        where TAggregate : IAggregateRoot
    { }
}
