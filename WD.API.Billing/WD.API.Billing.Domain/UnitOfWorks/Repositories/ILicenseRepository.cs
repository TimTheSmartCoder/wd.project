﻿using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;

namespace WD.API.Billing.Domain.UnitOfWorks.Repositories
{
    public interface ILicenseRepository
    {
        /// <summary>
        /// Deletes the given <see cref="License"/>.
        /// </summary>
        /// <param name="license"><see cref="License"/> to delete.</param>
        /// <returns>True if successfull, otherwise false.</returns>
        bool Delete(License license);

        /// <summary>
        /// Find's <see cref="License"/> with the given id.
        /// </summary>
        /// <param name="id">Id of the <see cref="License"/> to find.</param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> for cancellation.</param>
        /// <returns><see cref="License"/> if found, otherwise NULL.</returns>
        Task<License> FindAsync(int id, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Inserts the given <see cref="License"/>.
        /// </summary>
        /// <param name="license"><see cref="License"/> to insert.</param>
        /// <returns>True if successfull, otherwise false.</returns>
        bool Insert(License license);

        /// <summary>
        /// Updates the given <see cref="License"/>.
        /// </summary>
        /// <param name="license"><see cref="License"/> to update.</param>
        /// <returns>True if successfull, otherwise false.</returns>
        bool Update(License license);
    }
}
