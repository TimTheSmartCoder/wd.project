﻿using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;

namespace WD.API.Billing.Domain.UnitOfWorks.Repositories
{
    public interface ICustomerRepository
        : IRepository<Customer>
    {
        /// <summary>
        /// Deletes the given <see cref="Customer"/>.
        /// </summary>
        /// <param name="customer"><see cref="Customer"/> to delete.</param>
        /// <returns>True if successfull, otherwise false.</returns>
        bool Delete(Customer customer);

        /// <summary>
        /// Find's <see cref="Customer"/> with the given id.
        /// </summary>
        /// <param name="id">Id of the <see cref="Customer"/> to find.</param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> for cancellation.</param>
        /// <returns><see cref="Customer"/> if found, otherwise NULL.</returns>
        Task<Customer> FindAsync(int id, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Inserts the given <see cref="Customer"/>.
        /// </summary>
        /// <param name="customer"><see cref="Customer"/> to insert.</param>
        /// <returns>True if successfull, otherwise false.</returns>
        bool Insert(Customer customer);

        /// <summary>
        /// Updates the given <see cref="Customer"/>.
        /// </summary>
        /// <param name="customer"><see cref="Customer"/> to update.</param>
        /// <returns>True if successfull, otherwise false.</returns>
        bool Update(Customer customer);
    }
}
