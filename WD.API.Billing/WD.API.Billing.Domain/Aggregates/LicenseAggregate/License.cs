﻿using System;
using WD.API.Billing.Domain.Core;

namespace WD.API.Billing.Domain.Aggregates.LicenseAggregate
{
    public class License
        : IAggregateRoot
    {
        /// <summary>
        /// Construct's <see cref="License"/> with default information.
        /// (Only exist for EFCore support).
        /// </summary>
        private License() { }

        /// <summary>
        /// Construct's <see cref="License"/> with the given information.
        /// </summary>
        /// <param name="id">Id of the <see cref="License"/>.</param>
        /// <param name="customer">Id of the customer which owns the <see cref="License"/>.</param>
        /// <param name="status">User which the license is connected to.</param>
        /// <param name="activated">Activation time of the <see cref="License"/>.</param>
        /// <param name="cancelled">Cancellation time of the <see cref="License"/>.</param>
        /// <param name="licenseStatus">Status of the <see cref="License"/>.</param>
        /// <param name="type">Type of license.</param>
        /// <param name="modified">UTC time of last modification.</param>
        public License(int id, int customer, int user, DateTime activated, DateTime? cancelled, LicenseStatus status, LicenseType type, DateTime modified)
            : this()
        {
            if (id < 0)
                throw new ArgumentOutOfRangeException(nameof(id), "Can't be less than zero.");
            if (customer < 0)
                throw new ArgumentOutOfRangeException(nameof(customer), "Can't be less than zero.");

            Id = id;
            Customer = customer;
            User = user;
            Activated = activated;
            Cancelled = cancelled;
            Status = status;
            Type = type;
            Modified = modified;
        }

        /// <summary>
        /// Primary key of license.
        /// </summary>
        public int Key { get; private set; }

        /// <summary>
        /// Id of the <see cref="License"/>.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Id of the Customer who owns the license.
        /// </summary>
        public int Customer { get; private set; }
        
        /// <summary>
        /// Id of the user, which have the license.
        /// </summary>
        public int User { get; private set; }

        /// <summary>
        /// <see cref="DateTime"/> of the activation in UTC.
        /// </summary>
        public DateTime Activated { get; private set; }

        /// <summary>
        /// <see cref="DateTime"/> of cancellation in UTC.
        /// </summary>
        public DateTime? Cancelled { get; private set; }

        /// <summary>
        /// <see cref="LicenseStatus"/> of the <see cref="License"/>.
        /// </summary>
        public LicenseStatus Status { get; private set; }

        /// <summary>
        /// <see cref="LicenseType"/> of the <see cref="License"/>.
        /// </summary>
        public LicenseType Type { get; private set; }

        /// <summary>
        /// UTC time of when the <see cref="License"/> were modified.
        /// </summary>
        public DateTime Modified { get; private set; }

        /// <summary>
        /// Sets the customer of the <see cref="License"/>.
        /// </summary>
        /// <param name="customer">Id of the customer.</param>
        public void SetCustomer(int customer)
        {
            if (customer < 0)
                throw new ArgumentOutOfRangeException(nameof(customer), "Can't be less than zero.");

            Customer = customer;
        }

        public void SetUser(int user)
        {
            if (user < 0)
                throw new ArgumentOutOfRangeException(nameof(user), "Can't be less than zero.");

            User = user;
        }

        /// <summary>
        /// Set's the activation time of the <see cref="License"/>.
        /// </summary>
        /// <param name="activationTime">UTC time of activation.</param>
        public void SetActivated(DateTime activationTime)
        {
            Activated = activationTime;
        }

        /// <summary>
        /// Set's the cancellation time of the <see cref="License"/>.
        /// </summary>
        /// <param name="cancellationTime">UTC time of cancellation.</param>
        public void SetCancelled(DateTime? cancellationTime)
        {
            Cancelled = cancellationTime;
        }

        /// <summary>
        /// Set's the status of the <see cref="License"/>.
        /// </summary>
        /// <param name="status">Status of the <see cref="License"/>.</param>
        public void SetStatus(LicenseStatus status)
        {
            Status = status;
        }

        /// <summary>
        /// Set's the <see cref="LicenseType"/> of the <see cref="License"/>.
        /// </summary>
        /// <param name="type"></param>
        public void SetType(LicenseType type)
        {
            Type = type;
        }

        /// <summary>
        /// Set's the modification time of the <see cref="License"/>.
        /// </summary>
        /// <param name="modified">UTC time of last modification.</param>
        public void SetModified(DateTime modified)
        {
            Modified = modified;
        }

        public enum LicenseType
        {
            /// <summary>
            /// Mobile version license.
            /// </summary>
            Mobile = 10,

            /// <summary>
            /// Light version license.
            /// </summary>
            Light = 20,

            /// <summary>
            /// Pro version license.
            /// </summary>
            Pro = 30
        }

        public enum LicenseStatus
        {
            Activated = 10,
            Cancelled = 20,
        }
    }
}
