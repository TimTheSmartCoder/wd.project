﻿using System;
using WD.API.Billing.Domain.Core;

namespace WD.API.Billing.Domain.Aggregates.CustomerAggregate
{
    public class Customer
        : IAggregateRoot
    {
        /// <summary>
        /// Construct's <see cref="Customer"/> with default information.
        /// (Only exist for EFCore support.).
        /// </summary>
        private Customer() { }

        /// <summary>
        /// Construct's <see cref="Customer"/> with the given information.
        /// </summary>
        /// <param name="id">Id of the <see cref="Customer"/>.</param>
        /// <param name="name">Name of the <see cref="Customer"/>.</param>
        /// <param name="modified">UTC time of when <see cref="Customer"/> were modified.</param>
        public Customer(int id, string name, DateTime modified)
        {
            if (id < 0)
                throw new ArgumentOutOfRangeException(nameof(id), "Can't be less than zero.");
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            Id = id;
            Name = name;
            Modified = modified;
        }

        /// <summary>
        /// Primary key of <see cref="Customer"/>.
        /// </summary>
        public int Key { get; private set; }

        /// <summary>
        /// Id of the <see cref="Customer"/>.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Name of the <see cref="Customer"/>.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// UTC time of when <see cref="Customer"/> were modified.
        /// </summary>
        public DateTime Modified { get; private set; }

        /// <summary>
        /// Set's the name of <see cref="Customer"/>.
        /// </summary>
        /// <param name="name">Name of the <see cref="Customer"/>.</param>
        public void SetName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            Name = name;
        }

        /// <summary>
        /// Set's modified time of the <see cref="Customer"/>.
        /// </summary>
        /// <param name="modified">UTC time of when <see cref="Customer"/> were modified.</param>
        public void SetModified(DateTime modified)
        {
            Modified = modified;
        }
    }
}
