﻿namespace WD.API.Billing.Domain.Core
{
    /// <summary>
    /// Marker class for Aggregate roots.
    /// </summary>
    public interface IAggregateRoot
    { }
}
