﻿namespace WD.API.Billing.Domain.Core
{
    /// <summary>
    /// Marker interface for an Value Object.
    /// </summary>
    public interface IValueObject
    { }
}
