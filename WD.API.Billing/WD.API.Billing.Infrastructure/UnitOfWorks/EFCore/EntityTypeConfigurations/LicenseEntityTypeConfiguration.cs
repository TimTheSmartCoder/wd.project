﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;

namespace WD.API.Billing.Infrastructure.UnitOfWorks.EFCore.EntityTypeConfigurations
{
    public class LicenseEntityTypeConfiguration
        : IEntityTypeConfiguration<License>
    {
        public void Configure(EntityTypeBuilder<License> builder)
        {
            // Set primary key.
            builder.HasKey(x => x.Key);

            // Index.
            builder.HasIndex(x => x.Id).IsUnique();

            // Set Property rules.
            builder.Property(x => x.Activated).IsRequired(true);
            builder.Property(x => x.Cancelled).IsRequired(false);
            builder.Property(x => x.Customer).IsRequired(true);
            builder.Property(x => x.Status).IsRequired(true);
        }
    }
}
