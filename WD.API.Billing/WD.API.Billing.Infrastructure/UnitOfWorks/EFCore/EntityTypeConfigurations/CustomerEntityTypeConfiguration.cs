﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;

namespace WD.API.Billing.Infrastructure.UnitOfWorks.EFCore.EntityTypeConfigurations
{
    public class CustomerEntityTypeConfiguration
        : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            // Set primary key.
            builder.HasKey(x => x.Key);

            // Index.
            builder.HasIndex(x => x.Id).IsUnique();

            // Set Property rules.
            builder.Property(x => x.Name).IsRequired(true);
        }
    }
}
