﻿using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Domain.Aggregates.LicenseAggregate;
using WD.API.Billing.Domain.UnitOfWorks.Repositories;

namespace WD.API.Billing.Infrastructure.UnitOfWorks.EFCore.Repositories
{
    public class LicenseRepository
        : ILicenseRepository
    {
        /// <summary>
        /// <see cref="BillingDbContext"/> to use.
        /// </summary>
        private readonly BillingDbContext _billingDbContext;

        /// <summary>
        /// Construct's <see cref="LicenseRepository"/> for <see cref="License"/>.
        /// </summary>
        /// <param name="billingDbContext"><see cref="BillingDbContext"/> to use.</param>
        public LicenseRepository(BillingDbContext billingDbContext)
        {
            if (billingDbContext == null)
                throw new ArgumentNullException(nameof(billingDbContext));

            _billingDbContext = billingDbContext;
        }

        public bool Delete(License license)
        {
            if (license == null)
                throw new ArgumentNullException(nameof(license));

            _billingDbContext.Set<License>().Remove(license);

            return true;
        }

        public async Task<License> FindAsync(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _billingDbContext.Set<License>().FindAsync(id);
        }

        public bool Insert(License license)
        {
            if (license == null)
                throw new ArgumentNullException(nameof(license));

            _billingDbContext.Set<License>().Add(license);

            return true;
        }

        public bool Update(License license)
        {
            if (license == null)
                throw new ArgumentNullException(nameof(license));

            _billingDbContext.Set<License>().Update(license);

            return true;
        }
    }
}
