﻿using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Domain.Aggregates.CustomerAggregate;
using WD.API.Billing.Domain.UnitOfWorks.Repositories;

namespace WD.API.Billing.Infrastructure.UnitOfWorks.EFCore.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        /// <summary>
        /// <see cref="BillingDbContext"/> to use.
        /// </summary>
        private readonly BillingDbContext _billingDbContext;

        /// <summary>
        /// Construct's <see cref="CustomerRepository"/> for <see cref="Customer"/>.
        /// </summary>
        /// <param name="billingDbContext"><see cref="BillingDbContext"/> to use.</param>
        public CustomerRepository(BillingDbContext billingDbContext)
        {
            if (billingDbContext == null)
                throw new ArgumentNullException(nameof(billingDbContext));

            _billingDbContext = billingDbContext;
        }

        public bool Delete(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            _billingDbContext.Set<Customer>().Remove(customer);

            return true;
        }

        public Task<Customer> FindAsync(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _billingDbContext.Set<Customer>().FindAsync(id);
        }

        public bool Insert(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            _billingDbContext.Set<Customer>().Add(customer);

            return true;
        }

        public bool Update(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            _billingDbContext.Set<Customer>().Update(customer);

            return true;
        }
        
    }
}
