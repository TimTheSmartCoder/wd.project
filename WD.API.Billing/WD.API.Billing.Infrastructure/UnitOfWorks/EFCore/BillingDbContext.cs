﻿using Microsoft.EntityFrameworkCore;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore.EntityTypeConfigurations;

namespace WD.API.Billing.Infrastructure.UnitOfWorks.EFCore
{
    public class BillingDbContext
        : DbContext
    {
        /// <summary>
        /// Construct's <see cref="BillingDbContext"/> with the given information.
        /// </summary>
        /// <param name="options"><see cref="DbContextOptions"/> for the context.</param>
        public BillingDbContext(DbContextOptions options) 
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LicenseEntityTypeConfiguration());
        }
    }
}
