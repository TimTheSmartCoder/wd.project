﻿using System;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Domain.UnitOfWorks.Repositories;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore.Repositories;

namespace WD.API.Billing.Infrastructure.UnitOfWorks.EFCore
{
    public class UnitOfWork
        : IUnitOfWork
    {
        /// <summary>
        /// <see cref="BillingDbContext"/> to use.
        /// </summary>
        private readonly BillingDbContext _billingDbContext;

        /// <summary>
        /// Construct's <see cref="IUnitOfWork"/> with the given information.
        /// </summary>
        /// <param name="billingDbContext"><see cref="BillingDbContext"/> to use.</param>
        public UnitOfWork(BillingDbContext billingDbContext)
        {
            if (billingDbContext == null)
                throw new ArgumentNullException(nameof(billingDbContext));

            _billingDbContext = billingDbContext;

            // Create repository list with all available repositories.
            Repositories = new RepositoryList(billingDbContext);
        }

        public IRepositoryList Repositories { get; }

        public async Task<bool> SaveChanges(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                // Save all changes in the context.
                var result = await _billingDbContext.SaveChangesAsync(cancellationToken);

                return true;
            }
            catch(Exception exception)
            {
                // Throw an new exception that wraps the thrown exception.
                throw new UnitOfWorkException($"{nameof(UnitOfWork)}  failed to save changes.", exception);
            }
        }
    }

    public class RepositoryList
        : IRepositoryList
    {
        /// <summary>
        /// <see cref="BillingDbContext"/> to use in repositories.
        /// </summary>
        private readonly BillingDbContext _billingDbContext;

        /// <summary>
        /// Hidden field for <see cref="Customers"/>.
        /// </summary>
        private ICustomerRepository _customerRepository;

        /// <summary>
        /// Hidden field for <see cref="Licenses"/>.
        /// </summary>
        private ILicenseRepository _licenseRepository;

        /// <summary>
        /// Construct's <see cref="BillingDbContext"/> with given information.
        /// </summary>
        /// <param name="billingDbContext"><see cref="BillingDbContext"/> to use in repositories.</param>
        public RepositoryList(BillingDbContext billingDbContext)
        {
            if (billingDbContext == null)
                throw new ArgumentNullException(nameof(billingDbContext));

            _billingDbContext = billingDbContext;
        }

        public ICustomerRepository Customers =>
            _customerRepository ?? (_customerRepository = new CustomerRepository(_billingDbContext));

        public ILicenseRepository Licenses =>
            _licenseRepository ?? (_licenseRepository = new LicenseRepository(_billingDbContext));
    }
}
