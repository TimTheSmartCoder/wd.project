﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WD.API.Billing.Domain.UnitOfWorks;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;
using WD.API.Billing.Service.Services;

namespace WD.API.Billing.Service.Configurations
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection ConfigureServices(
            this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Add UnitOfWork to the services.
            serviceCollection.AddTransient<IUnitOfWork, UnitOfWork>();

            // Enabled background service, if enabled.
            if (bool.Parse(configuration["BackgroundServices:Enabled"]))
            {
                // Add background services.
                serviceCollection.AddHostedService<CustomerBackgroundService>();
                serviceCollection.AddHostedService<LicenseBackgroundService>();
            }

            return serviceCollection;
        }
    }
}
