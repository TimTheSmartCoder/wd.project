﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WD.API.Billing.Infrastructure.UnitOfWorks.EFCore;

namespace WD.API.Billing.Service.Configurations
{
    public static class HealthCheckConfiguration
    {
        /// <summary>
        /// Configure Health checks.
        /// </summary>
        public static IServiceCollection ConfigureHealthChecks(
            this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Add HealthCheck to the list of services.
            var healthChecks = serviceCollection.AddHealthChecks();

            // Add SQL server health check.
            healthChecks.AddSqlServer(configuration.GetConnectionString("Default"));

            // Add EFCore health check.
            healthChecks.AddDbContextCheck<BillingDbContext>();

            return serviceCollection;
        }

        /// <summary>
        /// Configure Health checks.
        /// </summary>
        public static IApplicationBuilder ConfigureHealthChecks(
            this IApplicationBuilder applicationBuilder, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            applicationBuilder.UseHealthChecks("/health");

            return applicationBuilder;
        }
    }
}
