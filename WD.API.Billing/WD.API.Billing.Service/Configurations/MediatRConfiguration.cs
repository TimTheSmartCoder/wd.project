﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WD.API.Billing.Application;

namespace WD.API.Billing.Service.Configurations
{
    public static class MediatRConfiguration
    {
        /// <summary>
        /// Configure MediatR.
        /// </summary>
        public static IServiceCollection ConfigureMediatR(
            this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            serviceCollection.AddMediatR(typeof(Assembly));

            return serviceCollection;
        }
    }
}
