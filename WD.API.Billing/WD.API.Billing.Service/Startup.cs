﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WD.API.Billing.Service.Configurations;

namespace WD.API.Billing.Service
{
    public class Startup
    {
        /// <summary>
        /// <see cref="IConfiguration"/> to use.
        /// </summary>
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Add Entity Framework Core configuration using extension.
            services.ConfigureEFCore(_configuration);

            // Add services configuration using extension.
            services.ConfigureServices(_configuration);

            // Add Health checks configuration using extension.
            services.ConfigureHealthChecks(_configuration);

            // Add Correlation Id configuration using extension.
            services.ConfigureCorrelationId(_configuration);

            // Add MediatR configuration using extension.
            services.ConfigureMediatR(_configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Add Health checks configuration using extension.
            app.ConfigureHealthChecks(_configuration);

            // Add Correlation id configuration using extension.
            app.ConfigureCorrelationId(_configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
