﻿using System;
using WD.API.Legacy.Domain.Enums;

namespace WD.API.Legacy.Application.Commands.Models
{
    public class EventScreenConnectModel
    {
        /// <summary>
        /// Id of the connection.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id of the user.
        /// </summary>
        public int User { get; set; }

        /// <summary>
        /// Id of the customer.
        /// </summary>
        public int Customer { get; set; }

        /// <summary>
        /// UTC time of connection.
        /// </summary>
        public DateTime Connected { get; set; }

        /// <summary>
        /// Type of license.
        /// </summary>
        public LicenseType Type { get; set; }
    }
}
