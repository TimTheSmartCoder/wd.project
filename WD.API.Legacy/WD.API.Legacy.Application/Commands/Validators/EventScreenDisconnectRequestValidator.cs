﻿using FluentValidation;
using WD.API.Legacy.Application.Commands.Requests;
using WD.API.Legacy.Domain.Enums;

namespace WD.API.Legacy.Application.Commands.Validators
{
    public class EventScreenDisconnectRequestValidator
        : AbstractValidator<EventScreenDisconnectRequest>
    {
        public EventScreenDisconnectRequestValidator()
        {
            RuleFor(x => x.Model)
                .NotNull()
                .WithMessage("Model is not supplied, but it is required.");

            When(x => x.Model != null, () => {

                RuleFor(x => x.Model.Id)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Id can't be less than zero.");

                RuleFor(x => x.Model.User)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("User can't be less than zero.");

                RuleFor(x => x.Model.Customer)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Customer can't be less than zero.");

                RuleFor(x => x.Model.Connected)
                    .NotNull()
                    .WithMessage("Connected is not supplied, but it is required.");

                RuleFor(x => x.Model.Type)
                    .NotEqual(LicenseType.Unknown)
                    .WithMessage("Type is an unkown type.");

                RuleFor(x => x.Model.Disconnected)
                    .NotNull()
                    .WithMessage("Disconnect is not supplied, but it is required.");
            });
        }
    }
}
