﻿using MediatR;
using System;
using WD.API.Legacy.Application.Commands.Models;
using WD.API.Legacy.Application.Commands.Results;

namespace WD.API.Legacy.Application.Commands.Requests
{
    public class EventScreenDisconnectRequest
        : IRequest<EventScreenDisconnectResult>
    {
        /// <summary>
        /// Construc's <see cref="EventScreenDisconnectRequest"/> with the given information.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        /// <param name="correlationId">Correlation id of the request.</param>
        public EventScreenDisconnectRequest(EventScreenDisconnectModel model, string correlationId)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            if (correlationId == null)
                throw new ArgumentNullException(nameof(correlationId));

            CorrelationId = correlationId;
            Model = model;
        }

        /// <summary>
        /// Correlation id of the request.
        /// </summary>
        public string CorrelationId { get; }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public EventScreenDisconnectModel Model { get; }
    }
}
