﻿using MediatR;
using System;
using WD.API.Legacy.Application.Commands.Models;
using WD.API.Legacy.Application.Commands.Results;

namespace WD.API.Legacy.Application.Commands.Requests
{
    public class EventScreenConnectRequest
        : IRequest<EventScreenConnectResult>
    {
        /// <summary>
        /// Construct's <see cref="EventScreenConnectRequest"/> with the given information.
        /// </summary>
        /// <param name="model">Model of the request.</param>
        /// <param name="correlationId">Correlation id of request.</param>
        public EventScreenConnectRequest(EventScreenConnectModel model, string correlationId)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            if (correlationId == null)
                throw new ArgumentNullException(nameof(correlationId));

            Model = model;
            CorrelationId = correlationId;
        }

        /// <summary>
        /// Correlation id of the request.
        /// </summary>
        public string CorrelationId { get; }

        /// <summary>
        /// Model of the request.
        /// </summary>
        public EventScreenConnectModel Model { get; }
    }
}
