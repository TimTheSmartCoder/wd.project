﻿using FluentValidation;
using MediatR;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WD.API.Legacy.Application.Commands.Requests;
using WD.API.Legacy.Application.Commands.Results;
using WD.API.Legacy.Application.Commands.Validators;

namespace WD.API.Legacy.Application.Commands.Handlers
{
    public class EventScreenDisconnectHandler
        : IRequestHandler<EventScreenDisconnectRequest, EventScreenDisconnectResult>
    {
        /// <summary>
        /// <see cref="IConfiguration"/> to use.
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// <see cref="IValidator"/> to use.
        /// </summary>
        private readonly IValidator<EventScreenDisconnectRequest> _validator;

        /// <summary>
        /// <see cref="ILogger"/> to use.
        /// </summary>
        private readonly ILogger<EventScreenDisconnectHandler> _logger;

        public EventScreenDisconnectHandler(IConfiguration configuration, ILogger<EventScreenDisconnectHandler> logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            _configuration = configuration;
            _logger = logger;

            // Set validator.
            _validator = new EventScreenDisconnectRequestValidator();
        }

        public async Task<EventScreenDisconnectResult> Handle(EventScreenDisconnectRequest request, CancellationToken cancellationToken)
        {
            var topicClient = new TopicClient(
                _configuration["Azure:ServiceBus:ConnectionStrings:Default"],
                "Screen");

            try
            {
                _logger.LogInformation("Staring processing request.");

                // Validate the request.
                _validator.ValidateAndThrow(request);

                _logger.LogInformation("Validated request successfully.");

                // Model of the request.
                var model = new
                {
                    Id = request.Model.Id,
                    User = request.Model.User,
                    Customer = request.Model.Customer,
                    Activated = request.Model.Connected,
                    Type = request.Model.Type,
                    Modified = request.Model.Connected,
                    Cancelled = request.Model.Disconnected
                };

                // Convert model to json.
                var modelAsJson = JsonConvert.SerializeObject(model);

                // Construct message with body.
                Message message = new Message(Encoding.UTF8.GetBytes(modelAsJson));

                // Set content type.
                message.ContentType = "application/json";

                // Set correlation id.
                message.CorrelationId = request.CorrelationId;

                // Add label.
                message.Label = "Released";

                _logger.LogInformation("Sending topic for disconnect screen.");

                // Send the message.
                await topicClient.SendAsync(message);

                _logger.LogInformation("Finished request successfully.");

            }
            finally
            {
                // close topic client.
                await topicClient.CloseAsync();
            }

            return new EventScreenDisconnectResult();
        }
    }
}
