﻿namespace WD.API.Legacy.Domain.Enums
{
    public enum LicenseType
    {
        /// <summary>
        /// Unknown type.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Mobile version license.
        /// </summary>
        Mobile = 10,

        /// <summary>
        /// Light version license.
        /// </summary>
        Light = 20,

        /// <summary>
        /// Pro version license.
        /// </summary>
        Pro = 30
    }
}
