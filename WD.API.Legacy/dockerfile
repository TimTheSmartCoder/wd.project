FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY WD.API.Legacy.Application/WD.API.Legacy.Application.csproj WD.API.Legacy.Application/
COPY WD.API.Legacy.Infrastructure/WD.API.Legacy.Infrastructure.csproj WD.API.Legacy.Infrastructure/
COPY WD.API.Legacy.Domain/WD.API.Legacy.Domain.csproj WD.API.Legacy.Domain/
COPY WD.API.Legacy.Service/WD.API.Legacy.Service.csproj WD.API.Legacy.Service/
RUN dotnet restore ./WD.API.Legacy.Service/WD.API.Legacy.Service.csproj

# Copy everything else and build
COPY . ./
RUN dotnet publish ./WD.API.Legacy.Service -c Debug -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/WD.API.Legacy.Service/out .
ENTRYPOINT ["dotnet", "WD.API.Legacy.Service.dll"]