﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WD.API.Legacy.Application.Commands.Models;
using WD.API.Legacy.Application.Commands.Requests;
using WD.API.Legacy.Application.Commands.Results;

namespace WD.API.Legacy.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        /// <summary>
        /// <see cref="IMediator"/> to use.
        /// </summary>
        private readonly IMediator _mediator;

        public EventsController(IMediator mediator)
        {
            if (mediator == null)
                throw new ArgumentNullException(nameof(mediator));

            _mediator = mediator;
        }

        [HttpPost("screen/connect")]
        public async Task<IActionResult> ScreenConnect(EventScreenConnectModel model)
        {
            // Process the request.
            EventScreenConnectResult result = await _mediator.Send(
                new EventScreenConnectRequest(model, Request.HttpContext.TraceIdentifier));

            return new OkObjectResult(result);
        }


        [HttpPost("screen/disconnect")]
        public async Task<IActionResult> ScreenDisconnect(EventScreenDisconnectModel model)
        {
            // Process the request.
            EventScreenDisconnectResult result = await _mediator.Send(
                new EventScreenDisconnectRequest(model, Request.HttpContext.TraceIdentifier));

            return new OkObjectResult(result);
        }
    }
}