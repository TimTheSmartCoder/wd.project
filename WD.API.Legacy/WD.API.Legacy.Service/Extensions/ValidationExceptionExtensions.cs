﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WD.API.Legacy.Service.Extensions
{
    public static class ValidationExceptionExtensions
    {
        /// <summary>
        /// Convert's the <see cref="ValidationException"/> to the model <see cref="ValidationExceptionModel"/>.
        /// </summary>
        /// <returns><see cref="ValidationExceptionModel"/> of the <see cref="ValidationException"/>.</returns>
        public static ValidationExceptionModel ConvertToModel(
            this ValidationException validationException)
        {
            // Get the list of errors from validation.
            var errors = validationException.Errors.Select(x => x.ErrorMessage);

            return new ValidationExceptionModel(errors);
        }

        public class ValidationExceptionModel
        {
            /// <summary>
            /// Construct's a <see cref="ValidationExceptionModel"/> with the given information.
            /// </summary>
            /// <param name="errors">List of errors.</param>
            public ValidationExceptionModel(IEnumerable<string> errors)
            {
                if (errors == null)
                    throw new ArgumentNullException(nameof(errors));

                Errors = errors;
            }

            /// <summary>
            /// List of errors.
            /// </summary>
            public IEnumerable<string> Errors { get; }
        }
    }
}
