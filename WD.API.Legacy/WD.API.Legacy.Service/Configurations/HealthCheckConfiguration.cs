﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace WD.API.Legacy.Service.Configurations
{
    public static class HealthCheckConfiguration
    {
        /// <summary>
        /// Configure's health check.
        /// </summary>
        public static IServiceCollection ConfigureHealthChecks(
            this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Add HealthCheck to the list of services.
            var healthChecks = serviceCollection.AddHealthChecks();

            // Health check for service bus.
            healthChecks.AddAzureServiceBusTopic(configuration["Azure:ServiceBus:ConnectionStrings:Default"], "Screen");

            return serviceCollection;
        }

        /// <summary>
        /// Configure's health check.
        /// </summary>
        public static IApplicationBuilder ConfigureHealthChecks(
            this IApplicationBuilder applicationBuilder, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            applicationBuilder.UseHealthChecks("/health");

            return applicationBuilder;
        }
    }
}
