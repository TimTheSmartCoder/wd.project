﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;
using WD.API.Legacy.Service.Extensions;

namespace WD.API.Legacy.Service.Middlewares
{
    public class BadRequestMiddleware
    {
        /// <summary>
        /// Reference to next middleware in the pipeline.
        /// </summary>
        private readonly RequestDelegate _next;

        /// <summary>
        /// Construct's <see cref="BadRequestMiddleware"/> with the given information.
        /// </summary>
        /// <param name="next">Next middleware to call.</param>
        public BadRequestMiddleware(RequestDelegate next)
        {
            if (next == null)
                throw new ArgumentNullException(nameof(next));

            _next = next;
        }

        public async Task Invoke(
            HttpContext httpContext,
            ILogger<BadRequestMiddleware> logger)
        {
            try
            {
                // Call the next middleware.
                await _next(httpContext);
            }
            catch (Exception exception) when (exception is ValidationException)
            {
                logger.LogWarning($"Detected {nameof(ValidationException)} responding with {HttpStatusCode.BadRequest}.");

                // Prepare response.
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                // Convert the exception to model.
                var model = (exception as ValidationException).ConvertToModel();

                // Convert model to json response.
                string response = JsonConvert.SerializeObject(model);

                // Write the response to the client.
                await httpContext.Response.WriteAsync(response);

            }
            catch (Exception exception)
            {
                logger.LogInformation($"Detected {nameof(Exception)} rethrowing the exception on to the next middleware.");

                // Re throw exception.
                throw exception;
            }
        }
    }
}
